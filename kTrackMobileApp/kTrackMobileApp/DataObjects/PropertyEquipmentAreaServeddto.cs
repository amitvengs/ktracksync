﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class PropertyEquipmentAreaServeddto : EntityData
    {
        public int property_equip_area_served_id { get; set; }
        //[NotMapped]
        public int property_equip_id { get; set; }
        //[NotMapped]
        public int property_loc_id { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public System.Guid rowguid { get; set; }

        public string MobilePropertyEquipId { get; set; }
        public string MobilePropertyLocId { get; set; }
    }
}