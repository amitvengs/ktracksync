﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class DocumentTypeEntityHierarchyDetailsdto : EntityData
    {
        public int document_type_entity_hierarchy_details_id { get; set; }

        public int entity_id { get; set; }

        public int document_type_hierarchy_id { get; set; }

        public bool is_enabled { get; set; }

        public int created_by_user_id { get; set; }

        public DateTime creation_date { get; set; }

        public int? document_info_id_temp { get; set; }
    }
}