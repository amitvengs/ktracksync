﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class Usersdto : EntityData
    {
        public int user_id { get; set; }
        public int user_type_id { get; set; }
        public int user_mapping_id { get; set; }
        public string user_login_id { get; set; }
        public bool is_active { get; set; }
        public string logged_in_location { get; set; }
        public bool is_system_account { get; set; }
        public Nullable<int> master_login_id { get; set; }
        public System.DateTime user_creation_date { get; set; }
        public int user_created_by { get; set; }
        public System.Guid rowguid { get; set; }
        //public bool DeletedMS { get; set; }
    }
}