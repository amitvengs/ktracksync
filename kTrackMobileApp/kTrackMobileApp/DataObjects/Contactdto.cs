﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class Contactdto : EntityData
    {
        public int contact_id { get; set; }
        public string contact_f_name { get; set; }
        public string contact_l_name { get; set; }
        public string contact_title { get; set; }
        public string contact_phone { get; set; }
        public string contact_ext { get; set; }
        public string contact_cell_phone { get; set; }
        public string contact_fax { get; set; }
        public string contact_email { get; set; }
        public string contact_add1 { get; set; }
        public string contact_add2 { get; set; }
        public string contact_city { get; set; }
        public string contact_state { get; set; }
        public string contact_zip { get; set; }
        public string contact_notes { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public System.Guid rowguid { get; set; }
    }
}