﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class ElectPanelCircuitdto : EntityData
    {
        public int elect_panel_circuit_id { get; set; }
        public int elect_panel_id { get; set; }
        public Nullable<int> elect_panel_bkr_id { get; set; }
        public int elect_panel_circuit_number { get; set; }
        public Nullable<double> elect_panel_bkr_kva_phase_a { get; set; }
        public Nullable<double> elect_panel_bkr_kva_phase_b { get; set; }
        public Nullable<double> elect_panel_bkr_kva_phase_c { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public System.Guid rowguid { get; set; }
    }
}