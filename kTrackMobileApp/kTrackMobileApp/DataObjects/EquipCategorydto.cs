﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class EquipCategorydto : EntityData
    {
        public int equip_category_id { get; set; }
        public string equip_category_name { get; set; }
        public string equip_category_acronym { get; set; }
        public bool equip_category_pm_approved { get; set; }
        public string equip_category_pm_approved_by { get; set; }
        public Nullable<System.DateTime> equip_category_pm_approved_date { get; set; }
        public int equip_category_parent { get; set; }
        public int equip_category_level { get; set; }
        public bool equip_category_is_maintained { get; set; }
        public bool equip_category_is_electrical_panel { get; set; }
        public bool equip_category_is_powered { get; set; }
        public bool equip_category_is_dco { get; set; }
        public bool equip_category_is_equip_cat { get; set; }
        public bool equip_category_has_start_date { get; set; }
        public bool equip_category_has_keys { get; set; }
        public Nullable<int> system_component_config_id { get; set; }
        public Nullable<int> color_id { get; set; }
        public Nullable<int> default_shape_id { get; set; }
        public Nullable<int> custom_shape_id { get; set; }
        public int validation_status_work_flow_id { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public System.Guid rowguid { get; set; }
        //public bool DeletedMS { get; set; }
    }
}