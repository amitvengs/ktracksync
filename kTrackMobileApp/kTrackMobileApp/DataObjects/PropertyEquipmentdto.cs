﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class PropertyEquipmentdto : EntityData
    {
        public int property_equip_id { get; set; }
        public int property_id { get; set; }
        public int equipment_id { get; set; }
        public string property_equip_serial_number { get; set; }
        public string property_equip_unit_number { get; set; }
        public string property_equip_revit_mark { get; set; }
        public Nullable<System.DateTime> property_equip_manufacture_date { get; set; }
        public Nullable<System.DateTime> property_equip_installation_date { get; set; }
        public bool property_equip_is_warranty { get; set; }
        public bool property_equip_is_assoc { get; set; }
        public string property_equip_notes { get; set; }
        public string property_equip_asset_tag { get; set; }
        public Nullable<System.DateTime> property_equip_expiration_date { get; set; }
        public int property_equip_status_id { get; set; }
        public int validation_status_id { get; set; }
        public bool property_equip_is_under_contract { get; set; }
        public Nullable<int> property_loc_id { get; set; }
        public Nullable<int> installed_by_contractor_id { get; set; }
        public Nullable<int> supplied_by_vendor_id { get; set; }
        public Nullable<int> revit_id { get; set; }
        public int origin_id { get; set; }
        public int created_by_user_id { get; set; }
        public System.DateTime creation_date { get; set; }
        public System.Guid rowguid { get; set; }
        public Nullable<System.DateTime> property_equip_start_date { get; set; }
        public int last_edited_by_user_id { get; set; }
        public System.DateTime last_edited_date { get; set; }
    }
}