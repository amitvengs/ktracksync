﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class PropertyBarcodedto : EntityData
    {
        public int barcode_id { get; set; }
        public string barcode { get; set; }
        public int mapping_id { get; set; }
        public int mapping_type_id { get; set; }
        public int property_id { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public System.Guid rowguid { get; set; }

        [Required]
        public string MobilePropertyId { get; set; }
        public string MobilePropertyEquipId { get; set; }
    }
}