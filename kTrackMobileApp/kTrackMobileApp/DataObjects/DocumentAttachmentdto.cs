﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class DocumentAttachmentdto : EntityData
    {
        public int document_attachment_id { get; set; }

        public DateTime? document_attachment_start_date { get; set; }

        public DateTime? document_attachment_end_date { get; set; }

        public bool document_attachment_is_expired { get; set; }

        public int document_id { get; set; }

        public int entity_mapping_id { get; set; }

        public int? document_info_id { get; set; }

        public int document_attachment_status_id { get; set; }

        public int created_by_user_id { get; set; }

        public DateTime creation_date { get; set; }

        //public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public int? entity_id { get; set; }

        public int? document_type_entity_hierarchy_details_id { get; set; }
    }
}