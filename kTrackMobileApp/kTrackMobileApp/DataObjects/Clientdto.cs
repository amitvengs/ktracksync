﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class Clientdto : EntityData
    {
        public int client_id { get; set; }

        public string client_name { get; set; }

        public bool is_active { get; set; }

        public int created_by_user_id { get; set; }

        public DateTime creation_date { get; set; }

        public Guid rowguid { get; set; }
    }
}