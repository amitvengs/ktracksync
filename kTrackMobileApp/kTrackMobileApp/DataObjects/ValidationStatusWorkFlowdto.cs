﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class ValidationStatusWorkFlowdto : EntityData
    {
        public int validation_status_work_flow_id { get; set; }
        public string validation_status_work_flow_name { get; set; }
        public bool is_default { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public System.Guid rowguid { get; set; }
    }
}