﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class ClientPropertydto : EntityData
    {
        public int cp_id { get; set; }
        public int client_id { get; set; }
        public int property_id { get; set; }
        public string cp_suit_apt_unit { get; set; }
        public string notes { get; set; }
        public bool is_active { get; set; }
        public int created_by_user_id { get; set; }
        public System.DateTime creation_date { get; set; }
        public System.Guid rowguid { get; set; }
    }
}