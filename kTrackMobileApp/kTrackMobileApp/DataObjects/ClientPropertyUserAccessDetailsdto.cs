﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class ClientPropertyUserAccessDetailsdto : EntityData
    {
        public int client_property_user_access_details_id { get; set; }
        public int client_property_id { get; set; }
        public int user_id { get; set; }
        public bool is_active { get; set; }
        public int created_by_user_id { get; set; }
        public System.DateTime creation_date { get; set; }
        public System.Guid rowguid { get; set; }
    }
}