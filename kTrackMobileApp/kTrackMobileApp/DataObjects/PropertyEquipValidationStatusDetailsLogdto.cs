﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class PropertyEquipValidationStatusDetailsLogdto : EntityData
    {
        public int property_equip_validation_status_details_log_id { get; set; }

        public int property_equip_id { get; set; }

        public int validation_status_id_from { get; set; }

        public int validation_status_id_to { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        public Guid rowguid { get; set; }        
    }
}