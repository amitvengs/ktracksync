﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class DocumentTypeHierarchydto : EntityData
    {
        public int document_type_hierarchy_id { get; set; }

        public int document_type_id { get; set; }

        public int parent_document_type_id { get; set; }

        public bool is_document_attachable { get; set; }

        public bool is_system_default { get; set; }

        public bool is_mobile_photo_enabled { get; set; }

        public int document_file_folder_property_details_id { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        //public bool deleted { get; set; }
    }
}