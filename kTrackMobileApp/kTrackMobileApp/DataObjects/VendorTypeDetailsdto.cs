﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class VendorTypeDetailsdto : EntityData
    {
        public int vendor_type_detail_id { get; set; }

        public int vendor_id { get; set; }

        public int vendor_type_id { get; set; }

        public int created_by_user_id { get; set; }

        public DateTime creation_date { get; set; }

        public Guid rowguid { get; set; }

        public string MobileVendorTypeId {get; set;}
    }
}