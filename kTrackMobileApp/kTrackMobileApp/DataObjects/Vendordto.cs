﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class Vendordto : EntityData
    {
        public int vendor_id { get; set; }
        public string vendor_name { get; set; }
        public string vendor_contact_name { get; set; }
        public string vendor_phone_number { get; set; }
        public string vendor_fax_number { get; set; }
        public string vendor_email_address { get; set; }
        public string vendor_address_1 { get; set; }
        public string vendor_address_2 { get; set; }
        public string vendor_address_city { get; set; }
        public string vendor_address_state { get; set; }
        public string vendor_address_zip { get; set; }
        public string vendor_web_site { get; set; }
        public int created_by_user_id { get; set; }
        public System.DateTime creation_date { get; set; }
        public int last_edited_by_user_id { get; set; }
        public System.DateTime last_edited_date { get; set; }
    }
}