﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class Origindto : EntityData
    {
        public int origin_id { get; set; }
        public string origin_name { get; set; }
        public bool origin_user_selection { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public System.Guid rowguid { get; set; }
        //public bool DeletedMS { get; set; }
    }
}