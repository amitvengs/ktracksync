﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class DocumentTypedto : EntityData
    {
        public int document_type_id { get; set; }

        public string document_type { get; set; }

        public string document_type_desc { get; set; }

        public int document_type_parent { get; set; }

        public int document_type_level { get; set; }

        public bool document_type_is_required { get; set; }

        public bool document_type_is_restricted { get; set; }

        public bool document_type_is_date_driven { get; set; }

        public bool document_type_requires_approval { get; set; }

        public bool document_type_is_warranty { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        //public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public bool is_document_type_category { get; set; }

        public bool is_system_default { get; set; }

        public string destination_path { get; set; }

        public int? precedence { get; set; }
    }
}