﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class VendorTypedto : EntityData
    {
        public int vendor_type_id { get; set; }

        public string vendor_type { get; set; }

        public int created_by_user_id { get; set; }

        public DateTime creation_date { get; set; }

        public Guid rowguid { get; set; }
    }
}