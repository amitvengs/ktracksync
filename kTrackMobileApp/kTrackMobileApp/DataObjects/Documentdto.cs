﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class Documentdto : EntityData
    {
        public int document_id { get; set; }

        public int property_id { get; set; }

        public string document_name { get; set; }

        public string document_format { get; set; }

        public string document_desc { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }
        
        //public Guid rowguid { get; set; }

        public string document_path { get; set; }
    }
}