﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileApp.DataObjects
{
    public class Equipmentdto : EntityData
    {
        public int equipment_id { get; set; }
        public string equipment_model_number { get; set; }
        public string equipment_notes { get; set; }
        public int equip_category_id { get; set; }
        public int equip_manufacturer_id { get; set; }
        public int origin_id { get; set; }
        public int validation_status_id { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public System.Guid rowguid { get; set; }
        public Nullable<int> revit_type_id { get; set; }
        public string revit_type_mark { get; set; }
        public Nullable<int> installed_by_contractor_id { get; set; }
        public Nullable<int> supplied_by_vendor_id { get; set; }
        public int last_edited_by_user_id { get; set; }
        public System.DateTime last_edited_date { get; set; }
    }
}