﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace kTrackMobileApp.DataObjects
{
    public class PropertyEquipValidationStatusDetailsdto : EntityData
    {
        public int property_equip_validation_status_details_id { get; set; }
        //[NotMapped]
        public int validation_status_id { get; set; }
        //[NotMapped]
        public int property_equip_id { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        public Guid rowguid { get; set; }

        public string MobilePropertyEquipId { get; set; }
        public string MobileValidationStatusId { get; set; }
    }
}