using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(kTrackMobileApp.Startup))]

namespace kTrackMobileApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureMobileApp(app);
        }
    }
}