﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class ClientPropertyUserAccessDetailsdtoController : TableController<ClientPropertyUserAccessDetailsdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<ClientPropertyUserAccessDetailsdto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<ClientPropertyUserAccessDetailsdto, tbl_client_property_user_access_details>(context, Request, tbl => tbl.Id);
        }

        // GET tables/ClientPropertyUserAccessDetailsdto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<ClientPropertyUserAccessDetailsdto> GetAllClientPropertyUserAccessDetailsdto()
        {
            return Query(); 
        }

        // GET tables/ClientPropertyUserAccessDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ClientPropertyUserAccessDetailsdto> GetClientPropertyUserAccessDetailsdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ClientPropertyUserAccessDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ClientPropertyUserAccessDetailsdto> PatchClientPropertyUserAccessDetailsdto(string id, Delta<ClientPropertyUserAccessDetailsdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/ClientPropertyUserAccessDetailsdto
        public async Task<IHttpActionResult> PostClientPropertyUserAccessDetailsdto(ClientPropertyUserAccessDetailsdto item)
        {
            ClientPropertyUserAccessDetailsdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/ClientPropertyUserAccessDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteClientPropertyUserAccessDetailsdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
