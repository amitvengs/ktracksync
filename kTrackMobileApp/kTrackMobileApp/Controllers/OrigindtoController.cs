﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class OrigindtoController : TableController<Origindto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<Origindto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<Origindto, tbl_origin>(context, Request, tbl => tbl.Id);
        }

        // GET tables/Origindto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<Origindto> GetAllOrigindto()
        {
            return Query(); 
        }

        // GET tables/Origindto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Origindto> GetOrigindto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Origindto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Origindto> PatchOrigindto(string id, Delta<Origindto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Origindto
        public async Task<IHttpActionResult> PostOrigindto(Origindto item)
        {
            Origindto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Origindto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteOrigindto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
