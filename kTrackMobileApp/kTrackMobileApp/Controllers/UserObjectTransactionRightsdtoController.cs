﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class UserObjectTransactionRightsdtoController : TableController<UserObjectTransactionRightsdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<UserObjectTransactionRightsdto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<UserObjectTransactionRightsdto, tbl_um_user_object_transaction_rights>(context, Request, tbl => tbl.Id);
        }

        // GET tables/UserObjectTransactionRightsdto
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<UserObjectTransactionRightsdto> GetAllUserObjectTransactionRightsdto()
        {
            return Query(); 
        }

        // GET tables/UserObjectTransactionRightsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<UserObjectTransactionRightsdto> GetUserObjectTransactionRightsdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/UserObjectTransactionRightsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<UserObjectTransactionRightsdto> PatchUserObjectTransactionRightsdto(string id, Delta<UserObjectTransactionRightsdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/UserObjectTransactionRightsdto
        public async Task<IHttpActionResult> PostUserObjectTransactionRightsdto(UserObjectTransactionRightsdto item)
        {
            UserObjectTransactionRightsdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/UserObjectTransactionRightsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteUserObjectTransactionRightsdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
