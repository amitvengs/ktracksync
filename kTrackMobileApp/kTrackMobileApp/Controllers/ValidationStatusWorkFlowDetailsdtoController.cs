﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class ValidationStatusWorkFlowDetailsdtoController : TableController<ValidationStatusWorkFlowDetailsdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<ValidationStatusWorkFlowDetailsdto>(context, Request);
            //DomainManager = new SimpleMappedEntityDomainManager<ValidationStatusWorkFlowDetailsdto, tbl_validation_status_work_flow_details>(context, Request, tbl => tbl.Id);
            DomainManager = new ValidationStatusWorkFlowDetailsDomainManager(context, Request);
        }

        // GET tables/ValidationStatusWorkFlowDetailsdto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<ValidationStatusWorkFlowDetailsdto> GetAllValidationStatusWorkFlowDetailsdto()
        {
            return Query(); 
        }

        // GET tables/ValidationStatusWorkFlowDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ValidationStatusWorkFlowDetailsdto> GetValidationStatusWorkFlowDetailsdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ValidationStatusWorkFlowDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ValidationStatusWorkFlowDetailsdto> PatchValidationStatusWorkFlowDetailsdto(string id, Delta<ValidationStatusWorkFlowDetailsdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/ValidationStatusWorkFlowDetailsdto
        public async Task<IHttpActionResult> PostValidationStatusWorkFlowDetailsdto(ValidationStatusWorkFlowDetailsdto item)
        {
            ValidationStatusWorkFlowDetailsdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/ValidationStatusWorkFlowDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteValidationStatusWorkFlowDetailsdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
