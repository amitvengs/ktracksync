﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class PropertyEquipValidationStatusDetailsLogdtoController : TableController<PropertyEquipValidationStatusDetailsLogdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<PropertyEquipValidationStatusDetailsLogdto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<PropertyEquipValidationStatusDetailsLogdto, tbl_property_equip_validation_status_details_log>(context, Request, tbl => tbl.Id);
        }

        // GET tables/PropertyEquipValidationStatusDetailsLogdto
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<PropertyEquipValidationStatusDetailsLogdto> GetAllPropertyEquipValidationStatusDetailsLogdto()
        {
            return Query(); 
        }

        // GET tables/PropertyEquipValidationStatusDetailsLogdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<PropertyEquipValidationStatusDetailsLogdto> GetPropertyEquipValidationStatusDetailsLogdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/PropertyEquipValidationStatusDetailsLogdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<PropertyEquipValidationStatusDetailsLogdto> PatchPropertyEquipValidationStatusDetailsLogdto(string id, Delta<PropertyEquipValidationStatusDetailsLogdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/PropertyEquipValidationStatusDetailsLogdto
        public async Task<IHttpActionResult> PostPropertyEquipValidationStatusDetailsLogdto(PropertyEquipValidationStatusDetailsLogdto item)
        {
            PropertyEquipValidationStatusDetailsLogdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/PropertyEquipValidationStatusDetailsLogdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletePropertyEquipValidationStatusDetailsLogdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
