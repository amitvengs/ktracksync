﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class ValidationStatusWorkFlowdtoController : TableController<ValidationStatusWorkFlowdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<ValidationStatusWorkFlowdto>(context, Request);
            //DomainManager = new SimpleMappedEntityDomainManager<ValidationStatusWorkFlowdto, tbl_validation_status_work_flow>(context, Request, tbl => tbl.Id);
            DomainManager = new ValidationStatusWorkFlowDomainManager(context, Request);
        }

        // GET tables/ValidationStatusWorkFlowdto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<ValidationStatusWorkFlowdto> GetAllValidationStatusWorkFlowdto()
        {
            return Query(); 
        }

        // GET tables/ValidationStatusWorkFlowdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ValidationStatusWorkFlowdto> GetValidationStatusWorkFlowdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ValidationStatusWorkFlowdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ValidationStatusWorkFlowdto> PatchValidationStatusWorkFlowdto(string id, Delta<ValidationStatusWorkFlowdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/ValidationStatusWorkFlowdto
        public async Task<IHttpActionResult> PostValidationStatusWorkFlowdto(ValidationStatusWorkFlowdto item)
        {
            ValidationStatusWorkFlowdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/ValidationStatusWorkFlowdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteValidationStatusWorkFlowdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
