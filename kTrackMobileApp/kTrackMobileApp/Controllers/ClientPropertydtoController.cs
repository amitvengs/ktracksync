﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class ClientPropertydtoController : TableController<ClientPropertydto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<ClientPropertydto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<ClientPropertydto, tbl_client_property>(context, Request, tbl => tbl.Id);
        }

        // GET tables/ClientPropertydto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<ClientPropertydto> GetAllClientPropertydto()
        {
            return Query(); 
        }

        // GET tables/ClientPropertydto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ClientPropertydto> GetClientPropertydto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ClientPropertydto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ClientPropertydto> PatchClientPropertydto(string id, Delta<ClientPropertydto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/ClientPropertydto
        public async Task<IHttpActionResult> PostClientPropertydto(ClientPropertydto item)
        {
            ClientPropertydto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/ClientPropertydto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteClientPropertydto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
