﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class PropertyBarcodedtoController : TableController<PropertyBarcodedto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<PropertyBarcodedto>(context, Request);
            //DomainManager = new SimpleMappedEntityDomainManager<PropertyBarcodedto, tbl_property_barcode>(context, Request, tbl => tbl.Id);
            DomainManager = new PropertyBarCodeDomainManager(context, Request);
        }

        // GET tables/PropertyBarcodedto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<PropertyBarcodedto> GetAllPropertyBarcodedto()
        {
            return Query(); 
        }

        // GET tables/PropertyBarcodedto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<PropertyBarcodedto> GetPropertyBarcodedto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/PropertyBarcodedto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<PropertyBarcodedto> PatchPropertyBarcodedto(string id, Delta<PropertyBarcodedto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/PropertyBarcodedto
        public async Task<IHttpActionResult> PostPropertyBarcodedto(PropertyBarcodedto item)
        {
            PropertyBarcodedto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/PropertyBarcodedto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletePropertyBarcodedto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
