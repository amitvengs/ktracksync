﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class DocumentAttachmentdtoController : TableController<DocumentAttachmentdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<DocumentAttachmentdto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<DocumentAttachmentdto, tbl_document_attachment>(context, Request, tbl => tbl.Id);
        }

        // GET tables/DocumentAttachmentdto
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<DocumentAttachmentdto> GetAllDocumentAttachmentdto()
        {
            return Query(); 
        }

        // GET tables/DocumentAttachmentdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<DocumentAttachmentdto> GetDocumentAttachmentdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/DocumentAttachmentdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<DocumentAttachmentdto> PatchDocumentAttachmentdto(string id, Delta<DocumentAttachmentdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/DocumentAttachmentdto
        public async Task<IHttpActionResult> PostDocumentAttachmentdto(DocumentAttachmentdto item)
        {
            DocumentAttachmentdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/DocumentAttachmentdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteDocumentAttachmentdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
