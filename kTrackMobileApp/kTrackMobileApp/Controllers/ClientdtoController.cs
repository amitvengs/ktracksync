﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class ClientdtoController : TableController<Clientdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<Clientdto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<Clientdto, tbl_client>(context, Request, tbl => tbl.Id);
        }

        // GET tables/Clientdto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<Clientdto> GetAllClientdto()
        {
            return Query(); 
        }

        // GET tables/Clientdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Clientdto> GetClientdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Clientdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Clientdto> PatchClientdto(string id, Delta<Clientdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Clientdto
        public async Task<IHttpActionResult> PostClientdto(Clientdto item)
        {
            Clientdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Clientdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteClientdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
