﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class PropertyEquipValidationStatusDetailsdtoController : TableController<PropertyEquipValidationStatusDetailsdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<PropertyEquipValidationStatusDetails>(context, Request);
            //DomainManager = new SimpleMappedEntityDomainManager<PropertyEquipValidationStatusDetailsdto, tbl_property_equip_validation_status_details>(context, Request, tbl => tbl.Id);
            DomainManager = new PropertyEquipValidationStatusDetailsDomainManager(context, Request);
        }

        // GET tables/PropertyEquipValidationStatusDetailsdto
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<PropertyEquipValidationStatusDetailsdto> GetAllPropertyEquipValidationStatusDetailsdto()
        {
            return Query(); 
        }

        // GET tables/PropertyEquipValidationStatusDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<PropertyEquipValidationStatusDetailsdto> GetPropertyEquipValidationStatusDetailsdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/PropertyEquipValidationStatusDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<PropertyEquipValidationStatusDetailsdto> PatchPropertyEquipValidationStatusDetailsdto(string id, Delta<PropertyEquipValidationStatusDetailsdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/PropertyEquipValidationStatusDetailsdto
        public async Task<IHttpActionResult> PostPropertyEquipValidationStatusDetailsdto(PropertyEquipValidationStatusDetailsdto item)
        {
            PropertyEquipValidationStatusDetailsdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/PropertyEquipValidationStatusDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletePropertyEquipValidationStatusDetailsdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
