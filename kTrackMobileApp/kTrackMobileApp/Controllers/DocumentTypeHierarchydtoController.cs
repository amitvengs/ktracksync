﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class DocumentTypeHierarchydtoController : TableController<DocumentTypeHierarchydto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<DocumentTypeHierarchydto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<DocumentTypeHierarchydto, tbl_document_type_hierarchy>(context, Request, tbl => tbl.Id);
        }

        // GET tables/DocumentTypeHierarchydto
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<DocumentTypeHierarchydto> GetAllDocumentTypeHierarchydto()
        {
            return Query(); 
        }

        // GET tables/DocumentTypeHierarchydto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<DocumentTypeHierarchydto> GetDocumentTypeHierarchydto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/DocumentTypeHierarchydto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<DocumentTypeHierarchydto> PatchDocumentTypeHierarchydto(string id, Delta<DocumentTypeHierarchydto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/DocumentTypeHierarchydto
        public async Task<IHttpActionResult> PostDocumentTypeHierarchydto(DocumentTypeHierarchydto item)
        {
            DocumentTypeHierarchydto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/DocumentTypeHierarchydto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteDocumentTypeHierarchydto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
