﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class PropertyLocationdtoController : TableController<PropertyLocationdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<PropertyLocationdto>(context, Request);
            //DomainManager = new SimpleMappedEntityDomainManager<PropertyLocationdto, tbl_property_loc>(context, Request, tbl => tbl.Id);
            DomainManager = new PropertyLocDomainManager(context, Request);
        }

        // GET tables/PropertyLocationdto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<PropertyLocationdto> GetAllPropertyLocationdto()
        {
            return Query(); 
        }

        // GET tables/PropertyLocationdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<PropertyLocationdto> GetPropertyLocationdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/PropertyLocationdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<PropertyLocationdto> PatchPropertyLocationdto(string id, Delta<PropertyLocationdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/PropertyLocationdto
        public async Task<IHttpActionResult> PostPropertyLocationdto(PropertyLocationdto item)
        {
            PropertyLocationdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/PropertyLocationdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletePropertyLocationdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
