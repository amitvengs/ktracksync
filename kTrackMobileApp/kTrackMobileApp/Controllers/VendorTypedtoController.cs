﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class VendorTypedtoController : TableController<VendorTypedto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<VendorTypedto>(context, Request);
            //DomainManager = new SimpleMappedEntityDomainManager<VendorTypedto, tbl_vendor_type>(context, Request, tbl => tbl.Id);
            DomainManager = new VendorTypeDomainManager(context, Request);
        }

        // GET tables/VendorTypedto
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<VendorTypedto> GetAllVendorTypedto()
        {
            return Query(); 
        }

        // GET tables/VendorTypedto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<VendorTypedto> GetVendorTypedto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/VendorTypedto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<VendorTypedto> PatchVendorTypedto(string id, Delta<VendorTypedto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/VendorTypedto
        public async Task<IHttpActionResult> PostVendorTypedto(VendorTypedto item)
        {
            VendorTypedto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/VendorTypedto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteVendorTypedto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
