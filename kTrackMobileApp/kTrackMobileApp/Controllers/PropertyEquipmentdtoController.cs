﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class PropertyEquipmentdtoController : TableController<PropertyEquipmentdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<PropertyEquipmentdto>(context, Request);
            //DomainManager = new SimpleMappedEntityDomainManager<PropertyEquipmentdto, tbl_property_equip>(context, Request, tbl => tbl.Id);
            DomainManager = new PropertyEquipDomainManager(context, Request);            
        }

        // GET tables/PropertyEquipmentdto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<PropertyEquipmentdto> GetAllPropertyEquipmentdto()
        {
            return Query(); 
        }

        // GET tables/PropertyEquipmentdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<PropertyEquipmentdto> GetPropertyEquipmentdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/PropertyEquipmentdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<PropertyEquipmentdto> PatchPropertyEquipmentdto(string id, Delta<PropertyEquipmentdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/PropertyEquipmentdto
        public async Task<IHttpActionResult> PostPropertyEquipmentdto(PropertyEquipmentdto item)
        {
            PropertyEquipmentdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/PropertyEquipmentdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletePropertyEquipmentdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
