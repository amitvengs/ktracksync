﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class ValidationStatusdtoController : TableController<ValidationStatusdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<ValidationStatusdto>(context, Request);
            //DomainManager = new SimpleMappedEntityDomainManager<ValidationStatusdto, tbl_validation_status>(context, Request, tbl => tbl.Id);
            DomainManager = new ValidationStatusDomainManager(context, Request);
        }

        // GET tables/ValidationStatusdto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<ValidationStatusdto> GetAllValidationStatusdto()
        {
            return Query(); 
        }

        // GET tables/ValidationStatusdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ValidationStatusdto> GetValidationStatusdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ValidationStatusdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ValidationStatusdto> PatchValidationStatusdto(string id, Delta<ValidationStatusdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/ValidationStatusdto
        public async Task<IHttpActionResult> PostValidationStatusdto(ValidationStatusdto item)
        {
            ValidationStatusdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/ValidationStatusdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteValidationStatusdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
