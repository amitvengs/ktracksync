﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class PropertyEquipElectPanelBkrController : TableController<PropertyEquipElectPanelBkrdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<PropertyEquipElectPanelBkr>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<PropertyEquipElectPanelBkrdto, tbl_property_equip_elect_panel_bkr>(context, Request, tbl => tbl.Id);
        }

        // GET tables/PropertyEquipElectPanelBkr
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<PropertyEquipElectPanelBkrdto> GetAllPropertyEquipElectPanelBkr()
        {
            return Query(); 
        }

        // GET tables/PropertyEquipElectPanelBkr/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<PropertyEquipElectPanelBkrdto> GetPropertyEquipElectPanelBkr(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/PropertyEquipElectPanelBkr/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<PropertyEquipElectPanelBkrdto> PatchPropertyEquipElectPanelBkr(string id, Delta<PropertyEquipElectPanelBkrdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/PropertyEquipElectPanelBkr
        public async Task<IHttpActionResult> PostPropertyEquipElectPanelBkr(PropertyEquipElectPanelBkrdto item)
        {
            PropertyEquipElectPanelBkrdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/PropertyEquipElectPanelBkr/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletePropertyEquipElectPanelBkr(string id)
        {
             return DeleteAsync(id);
        }
    }
}
