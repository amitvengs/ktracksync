﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class ElectPanelBkrSupplyTypedtoController : TableController<ElectPanelBkrSupplyTypedto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<ElectPanelBkrSupplyTypedto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<ElectPanelBkrSupplyTypedto, tbl_elect_panel_bkr_supply_type>(context, Request, tbl => tbl.Id);
        }

        // GET tables/ElectPanelBkrSupplyTypedto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<ElectPanelBkrSupplyTypedto> GetAllElectPanelBkrSupplyTypedto()
        {
            return Query(); 
        }

        // GET tables/ElectPanelBkrSupplyTypedto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ElectPanelBkrSupplyTypedto> GetElectPanelBkrSupplyTypedto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ElectPanelBkrSupplyTypedto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ElectPanelBkrSupplyTypedto> PatchElectPanelBkrSupplyTypedto(string id, Delta<ElectPanelBkrSupplyTypedto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/ElectPanelBkrSupplyTypedto
        public async Task<IHttpActionResult> PostElectPanelBkrSupplyTypedto(ElectPanelBkrSupplyTypedto item)
        {
            ElectPanelBkrSupplyTypedto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/ElectPanelBkrSupplyTypedto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteElectPanelBkrSupplyTypedto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
