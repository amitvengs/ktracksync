﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class VendordtoController : TableController<Vendordto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<Vendordto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<Vendordto, tbl_vendor>(context, Request, tbl => tbl.Id);
        }

        // GET tables/Vendordto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<Vendordto> GetAllVendordto()
        {
            return Query(); 
        }

        // GET tables/Vendordto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Vendordto> GetVendordto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Vendordto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Vendordto> PatchVendordto(string id, Delta<Vendordto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Vendordto
        public async Task<IHttpActionResult> PostVendordto(Vendordto item)
        {
            Vendordto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Vendordto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteVendordto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
