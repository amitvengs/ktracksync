﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class DocumentTypedtoController : TableController<DocumentTypedto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<DocumentTypedto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<DocumentTypedto, tbl_document_type>(context, Request, tbl => tbl.Id);
        }

        // GET tables/DocumentTypedto
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<DocumentTypedto> GetAllDocumentTypedto()
        {
            return Query(); 
        }

        // GET tables/DocumentTypedto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<DocumentTypedto> GetDocumentTypedto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/DocumentTypedto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<DocumentTypedto> PatchDocumentTypedto(string id, Delta<DocumentTypedto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/DocumentTypedto
        public async Task<IHttpActionResult> PostDocumentTypedto(DocumentTypedto item)
        {
            DocumentTypedto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/DocumentTypedto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteDocumentTypedto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
