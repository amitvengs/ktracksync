﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class EquipCategorydtoController : TableController<EquipCategorydto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<EquipCategorydto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<EquipCategorydto, tbl_equip_category>(context, Request, tbl => tbl.Id);
        }

        // GET tables/EquipCategorydto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<EquipCategorydto> GetAllEquipCategorydto()
        {
            return Query(); 
        }

        // GET tables/EquipCategorydto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<EquipCategorydto> GetEquipCategorydto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/EquipCategorydto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<EquipCategorydto> PatchEquipCategorydto(string id, Delta<EquipCategorydto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/EquipCategorydto
        public async Task<IHttpActionResult> PostEquipCategorydto(EquipCategorydto item)
        {
            EquipCategorydto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/EquipCategorydto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteEquipCategorydto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
