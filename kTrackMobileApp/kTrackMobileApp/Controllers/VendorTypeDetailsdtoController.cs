﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class VendorTypeDetailsdtoController : TableController<VendorTypeDetailsdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<VendorTypeDetailsdto>(context, Request);
            //DomainManager = new SimpleMappedEntityDomainManager<VendorTypeDetailsdto, tbl_vendor_type_details>(context, Request, tbl => tbl.Id);
            DomainManager = new VendorTypeDetailsDomainManager(context, Request);
        }

        // GET tables/VendorTypeDetailsdto
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<VendorTypeDetailsdto> GetAllVendorTypeDetailsdto()
        {
            return Query(); 
        }

        // GET tables/VendorTypeDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<VendorTypeDetailsdto> GetVendorTypeDetailsdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/VendorTypeDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<VendorTypeDetailsdto> PatchVendorTypeDetailsdto(string id, Delta<VendorTypeDetailsdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/VendorTypeDetailsdto
        public async Task<IHttpActionResult> PostVendorTypeDetailsdto(VendorTypeDetailsdto item)
        {
            VendorTypeDetailsdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/VendorTypeDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteVendorTypeDetailsdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
