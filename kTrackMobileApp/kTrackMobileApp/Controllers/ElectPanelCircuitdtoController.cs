﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class ElectPanelCircuitdtoController : TableController<ElectPanelCircuitdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<ElectPanelCircuitdto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<ElectPanelCircuitdto, tbl_elect_panel_circuit>(context, Request, tbl => tbl.Id);
        }

        // GET tables/ElectPanelCircuitdto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<ElectPanelCircuitdto> GetAllElectPanelCircuitdto()
        {
            return Query(); 
        }

        // GET tables/ElectPanelCircuitdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ElectPanelCircuitdto> GetElectPanelCircuitdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ElectPanelCircuitdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ElectPanelCircuitdto> PatchElectPanelCircuitdto(string id, Delta<ElectPanelCircuitdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/ElectPanelCircuitdto
        public async Task<IHttpActionResult> PostElectPanelCircuitdto(ElectPanelCircuitdto item)
        {
            ElectPanelCircuitdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/ElectPanelCircuitdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteElectPanelCircuitdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
