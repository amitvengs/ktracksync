﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class PropertyProjectdtoController : TableController<PropertyProjectdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<PropertyProjectdto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<PropertyProjectdto, tbl_property_project>(context, Request, tbl => tbl.Id);
        }

        // GET tables/PropertyProjectdto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<PropertyProjectdto> GetAllPropertyProjectdto()
        {
            return Query(); 
        }

        // GET tables/PropertyProjectdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<PropertyProjectdto> GetPropertyProjectdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/PropertyProjectdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<PropertyProjectdto> PatchPropertyProjectdto(string id, Delta<PropertyProjectdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/PropertyProjectdto
        public async Task<IHttpActionResult> PostPropertyProjectdto(PropertyProjectdto item)
        {
            PropertyProjectdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/PropertyProjectdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletePropertyProjectdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
