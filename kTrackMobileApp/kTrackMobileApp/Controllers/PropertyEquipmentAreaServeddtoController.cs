﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class PropertyEquipmentAreaServeddtoController : TableController<PropertyEquipmentAreaServeddto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<PropertyEquipmentAreaServeddto>(context, Request);
            //DomainManager = new SimpleMappedEntityDomainManager<PropertyEquipmentAreaServeddto, tbl_property_equip_area_served>(context, Request, tbl => tbl.Id);
            DomainManager = new PropertyEquipAreaServedDomainManager(context, Request);
        }

        // GET tables/PropertyEquipmentAreaServeddto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<PropertyEquipmentAreaServeddto> GetAllPropertyEquipmentAreaServeddto()
        {
            return Query(); 
        }

        // GET tables/PropertyEquipmentAreaServeddto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<PropertyEquipmentAreaServeddto> GetPropertyEquipmentAreaServeddto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/PropertyEquipmentAreaServeddto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<PropertyEquipmentAreaServeddto> PatchPropertyEquipmentAreaServeddto(string id, Delta<PropertyEquipmentAreaServeddto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/PropertyEquipmentAreaServeddto
        public async Task<IHttpActionResult> PostPropertyEquipmentAreaServeddto(PropertyEquipmentAreaServeddto item)
        {
            PropertyEquipmentAreaServeddto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/PropertyEquipmentAreaServeddto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletePropertyEquipmentAreaServeddto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
