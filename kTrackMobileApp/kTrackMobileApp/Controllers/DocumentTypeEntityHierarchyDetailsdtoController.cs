﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class DocumentTypeEntityHierarchyDetailsdtoController : TableController<DocumentTypeEntityHierarchyDetailsdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<DocumentTypeEntityHierarchyDetailsdto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<DocumentTypeEntityHierarchyDetailsdto, tbl_document_type_entity_hierarchy_details>(context, Request, tbl => tbl.Id);
        }

        // GET tables/DocumentTypeEntityHierarchyDetailsdto
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<DocumentTypeEntityHierarchyDetailsdto> GetAllDocumentTypeEntityHierarchyDetailsdto()
        {
            return Query(); 
        }

        // GET tables/DocumentTypeEntityHierarchyDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<DocumentTypeEntityHierarchyDetailsdto> GetDocumentTypeEntityHierarchyDetailsdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/DocumentTypeEntityHierarchyDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<DocumentTypeEntityHierarchyDetailsdto> PatchDocumentTypeEntityHierarchyDetailsdto(string id, Delta<DocumentTypeEntityHierarchyDetailsdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/DocumentTypeEntityHierarchyDetailsdto
        public async Task<IHttpActionResult> PostDocumentTypeEntityHierarchyDetailsdto(DocumentTypeEntityHierarchyDetailsdto item)
        {
            DocumentTypeEntityHierarchyDetailsdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/DocumentTypeEntityHierarchyDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteDocumentTypeEntityHierarchyDetailsdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
