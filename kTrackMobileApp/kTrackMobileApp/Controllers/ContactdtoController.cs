﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class ContactdtoController : TableController<Contactdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<Contactdto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<Contactdto, tbl_contact>(context, Request, tbl => tbl.Id);
        }

        // GET tables/Contactdto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<Contactdto> GetAllContactdto()
        {
            return Query(); 
        }

        // GET tables/Contactdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Contactdto> GetContactdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Contactdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Contactdto> PatchContactdto(string id, Delta<Contactdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Contactdto
        public async Task<IHttpActionResult> PostContactdto(Contactdto item)
        {
            Contactdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Contactdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteContactdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
