﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class ElectPaneldtoController : TableController<ElectPaneldto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<ElectPaneldto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<ElectPaneldto, tbl_elect_panel>(context, Request, tbl => tbl.Id);
        }

        // GET tables/ElectPaneldto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<ElectPaneldto> GetAllElectPaneldto()
        {
            return Query(); 
        }

        // GET tables/ElectPaneldto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ElectPaneldto> GetElectPaneldto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ElectPaneldto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ElectPaneldto> PatchElectPaneldto(string id, Delta<ElectPaneldto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/ElectPaneldto
        public async Task<IHttpActionResult> PostElectPaneldto(ElectPaneldto item)
        {
            ElectPaneldto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/ElectPaneldto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteElectPaneldto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
