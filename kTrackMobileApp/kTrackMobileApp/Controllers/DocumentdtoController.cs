﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class DocumentdtoController : TableController<Documentdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<Documentdto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<Documentdto, tbl_document>(context, Request, tbl => tbl.Id);
        }

        // GET tables/Documentdto
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<Documentdto> GetAllDocumentdto()
        {
            return Query(); 
        }

        // GET tables/Documentdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Documentdto> GetDocumentdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Documentdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Documentdto> PatchDocumentdto(string id, Delta<Documentdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Documentdto
        public async Task<IHttpActionResult> PostDocumentdto(Documentdto item)
        {
            Documentdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Documentdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteDocumentdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
