﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class PropertyProjectUserAccessDetailsdtoController : TableController<PropertyProjectUserAccessDetailsdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<PropertyProjectUserAccessDetailsdto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<PropertyProjectUserAccessDetailsdto, tbl_property_project_user_access_details>(context, Request, tbl => tbl.Id);
        }

        // GET tables/PropertyProjectUserAccessDetailsdto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<PropertyProjectUserAccessDetailsdto> GetAllPropertyProjectUserAccessDetailsdto()
        {
            return Query(); 
        }

        // GET tables/PropertyProjectUserAccessDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<PropertyProjectUserAccessDetailsdto> GetPropertyProjectUserAccessDetailsdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/PropertyProjectUserAccessDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<PropertyProjectUserAccessDetailsdto> PatchPropertyProjectUserAccessDetailsdto(string id, Delta<PropertyProjectUserAccessDetailsdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/PropertyProjectUserAccessDetailsdto
        public async Task<IHttpActionResult> PostPropertyProjectUserAccessDetailsdto(PropertyProjectUserAccessDetailsdto item)
        {
            PropertyProjectUserAccessDetailsdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/PropertyProjectUserAccessDetailsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletePropertyProjectUserAccessDetailsdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
