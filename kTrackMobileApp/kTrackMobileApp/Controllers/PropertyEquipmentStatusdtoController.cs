﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class PropertyEquipmentStatusdtoController : TableController<PropertyEquipmentStatusdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<PropertyEquipmentStatusdto>(context, Request);
            //DomainManager = new SimpleMappedEntityDomainManager<PropertyEquipmentStatusdto, tbl_property_equip_status>(context, Request, tbl => tbl.Id);
            DomainManager = new PropertyEquipStatusDomainManager(context, Request);
        }

        // GET tables/PropertyEquipmentStatusdto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<PropertyEquipmentStatusdto> GetAllPropertyEquipmentStatusdto()
        {
            return Query(); 
        }

        // GET tables/PropertyEquipmentStatusdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<PropertyEquipmentStatusdto> GetPropertyEquipmentStatusdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/PropertyEquipmentStatusdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<PropertyEquipmentStatusdto> PatchPropertyEquipmentStatusdto(string id, Delta<PropertyEquipmentStatusdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/PropertyEquipmentStatusdto
        public async Task<IHttpActionResult> PostPropertyEquipmentStatusdto(PropertyEquipmentStatusdto item)
        {
            PropertyEquipmentStatusdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/PropertyEquipmentStatusdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletePropertyEquipmentStatusdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
