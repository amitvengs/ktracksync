﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class EquipmentdtoController : TableController<Equipmentdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<Equipmentdto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<Equipmentdto, tbl_equipment>(context, Request, tbl => tbl.Id);
        }

        // GET tables/Equipmentdto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<Equipmentdto> GetAllEquipmentdto()
        {
            return Query(); 
        }

        // GET tables/Equipmentdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Equipmentdto> GetEquipmentdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Equipmentdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Equipmentdto> PatchEquipmentdto(string id, Delta<Equipmentdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Equipmentdto
        public async Task<IHttpActionResult> PostEquipmentdto(Equipmentdto item)
        {
            Equipmentdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Equipmentdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteEquipmentdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
