﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class ElectPanelBkrdtoController : TableController<ElectPanelBkrdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            //DomainManager = new EntityDomainManager<ElectPanelBkrdto>(context, Request);
            DomainManager = new SimpleMappedEntityDomainManager<ElectPanelBkrdto, tbl_elect_panel_bkr>(context, Request, tbl => tbl.Id);
        }

        // GET tables/ElectPanelBkrdto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<ElectPanelBkrdto> GetAllElectPanelBkrdto()
        {
            return Query(); 
        }

        // GET tables/ElectPanelBkrdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ElectPanelBkrdto> GetElectPanelBkrdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ElectPanelBkrdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ElectPanelBkrdto> PatchElectPanelBkrdto(string id, Delta<ElectPanelBkrdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/ElectPanelBkrdto
        public async Task<IHttpActionResult> PostElectPanelBkrdto(ElectPanelBkrdto item)
        {
            ElectPanelBkrdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/ElectPanelBkrdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteElectPanelBkrdto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
