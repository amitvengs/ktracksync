﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using kTrackMobileApp.Helper;

namespace kTrackMobileApp.Controllers
{
    public class PropertydtoController : TableController<Propertydto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackContext context = new kTrackContext();
            DomainManager = new SimpleMappedEntityDomainManager<Propertydto, tbl_property>(context, Request, tbl => tbl.Id);
        }

        // GET tables/Propertydto
        //[EnableQuery(MaxTop = 1000)]
        [EnableQuery(PageSize = Constant.MAXPAGESIZE)]
        public IQueryable<Propertydto> GetAllPropertydto()
        {
            return Query(); 
        }

        // GET tables/Propertydto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Propertydto> GetPropertydto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Propertydto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Propertydto> PatchPropertydto(string id, Delta<Propertydto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Propertydto
        public async Task<IHttpActionResult> PostPropertydto(Propertydto item)
        {
            Propertydto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Propertydto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletePropertydto(string id)
        {
             return DeleteAsync(id);
        }
    }
}
