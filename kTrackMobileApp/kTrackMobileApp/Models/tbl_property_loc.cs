namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_property_loc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_property_loc()
        {
            tbl_property_equip = new HashSet<tbl_property_equip>();
            tbl_property_equip_area_served = new HashSet<tbl_property_equip_area_served>();
        }

        [Key]
        public int property_loc_id { get; set; }

        public int property_id { get; set; }

        [StringLength(255)]
        public string property_loc_name { get; set; }

        [Required]
        [StringLength(255)]
        public string property_loc_number { get; set; }

        [StringLength(255)]
        public string property_loc_const_number { get; set; }

        public double? property_loc_volume { get; set; }

        public double? property_loc_perimiter { get; set; }

        public double? property_loc_area { get; set; }

        public double? property_loc_ceiling_height { get; set; }

        public double? property_loc_occupancy_factor { get; set; }

        [StringLength(150)]
        public string property_loc_sign_message { get; set; }

        public int? property_department_id { get; set; }

        public int property_loc_level_id { get; set; }

        public int? property_loc_type_id { get; set; }

        public int? property_loc_zone_id { get; set; }

        public int origin_id { get; set; }

        public int? revit_id { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public int last_edited_by_user_id { get; set; }

        public DateTime last_edited_date { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public virtual tbl_origin tbl_origin { get; set; }

        public virtual tbl_property tbl_property { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip> tbl_property_equip { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip_area_served> tbl_property_equip_area_served { get; set; }

        public virtual tbl_um_users tbl_um_users { get; set; }
    }
}
