namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_equipment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_equipment()
        {
            tbl_property_equip = new HashSet<tbl_property_equip>();
        }

        [Key]
        public int equipment_id { get; set; }

        [Required]
        [StringLength(250)]
        public string equipment_model_number { get; set; }

        [Column(TypeName = "ntext")]
        public string equipment_notes { get; set; }

        public int equip_category_id { get; set; }

        public int equip_manufacturer_id { get; set; }

        public int origin_id { get; set; }

        public int validation_status_id { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public int? revit_type_id { get; set; }

        [StringLength(50)]
        public string revit_type_mark { get; set; }

        public int? installed_by_contractor_id { get; set; }

        public int? supplied_by_vendor_id { get; set; }

        public int last_edited_by_user_id { get; set; }

        public DateTime last_edited_date { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public virtual tbl_equip_category tbl_equip_category { get; set; }

        public virtual tbl_origin tbl_origin { get; set; }

        public virtual tbl_um_users tbl_um_users { get; set; }

        public virtual tbl_validation_status tbl_validation_status { get; set; }

        public virtual tbl_vendor tbl_vendor { get; set; }

        public virtual tbl_vendor tbl_vendor1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip> tbl_property_equip { get; set; }
    }
}
