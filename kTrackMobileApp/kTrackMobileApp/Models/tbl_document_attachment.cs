namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_document_attachment
    {
        [Key]
        public int document_attachment_id { get; set; }

        [Column(TypeName = "date")]
        public DateTime? document_attachment_start_date { get; set; }

        [Column(TypeName = "date")]
        public DateTime? document_attachment_end_date { get; set; }

        public bool document_attachment_is_expired { get; set; }

        public int document_id { get; set; }

        public int entity_mapping_id { get; set; }

        public int? document_info_id { get; set; }

        public int document_attachment_status_id { get; set; }

        public int created_by_user_id { get; set; }

        public DateTime creation_date { get; set; }

        public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public int? entity_id { get; set; }

        public int? document_type_entity_hierarchy_details_id { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public virtual tbl_document tbl_document { get; set; }
    }
}
