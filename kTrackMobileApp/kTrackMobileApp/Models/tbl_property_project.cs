namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_property_project
    {
        [Key]
        public int property_project_id { get; set; }

        public int cp_id { get; set; }

        [StringLength(50)]
        public string property_project_number { get; set; }

        [Required]
        [StringLength(50)]
        public string property_project_name { get; set; }

        public string property_project_desc { get; set; }

        [StringLength(50)]
        public string property_project_cm_gc_number { get; set; }

        public int property_project_status_id { get; set; }

        public int entity_work_flow_id { get; set; }

        public int last_edited_by_user_id { get; set; }

        public DateTime last_edited_date { get; set; }

        public int created_by_user_id { get; set; }

        public DateTime creation_date { get; set; }

        public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public virtual tbl_client_property tbl_client_property { get; set; }

        public virtual tbl_um_users tbl_um_users { get; set; }

        public virtual tbl_um_users tbl_um_users1 { get; set; }
    }
}
