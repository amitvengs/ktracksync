﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using kTrackMobileApp.DataObjects;
using Microsoft.Azure.Mobile.Server;
using System.Net.Http;
using System.Data.Entity;
using System.Web.Http;
using System.Threading.Tasks;
using AutoMapper;
using System.Web.Http.OData;

namespace kTrackMobileApp.Models
{
    public class PropertyEquipDomainManager : MappedEntityDomainManager<PropertyEquipmentdto, tbl_property_equip>
    {
        private kTrackContext context;

        public PropertyEquipDomainManager(kTrackContext context, HttpRequestMessage request)
            : base(context, request, true)
        {
            Request = request;
            this.context = context;
        }

        public static int GetKey(string mobilepropertyequipId, DbSet<tbl_property_equip> propertyequip, HttpRequestMessage request)
        {
            int propertyequipId = propertyequip
               .Where(c => c.Id == mobilepropertyequipId)
               .Select(c => c.property_equip_id)
               .FirstOrDefault();

            if (propertyequipId == 0)
            {
                throw new HttpResponseException(request.CreateNotFoundResponse());
            }
            return propertyequipId;
        }

        protected override T GetKey<T>(string mobilepropertyequipId)
        {
            return (T)(object)GetKey(mobilepropertyequipId, this.context.tbl_property_equip, this.Request);
        }

        public override SingleResult<PropertyEquipmentdto> Lookup(string mobilepropertyequipId)
        {
            int propertyequipId = GetKey<int>(mobilepropertyequipId);
            return LookupEntity(c => c.property_equip_id == propertyequipId);
        }

        public override async Task<PropertyEquipmentdto> InsertAsync(PropertyEquipmentdto mobilePropertyEquip)
        {
            //if (mobilePropertyEquip.property_equip_manufacture_date.HasValue)
            //{
            //    mobilePropertyEquip.property_equip_manufacture_date = mobilePropertyEquip.property_equip_manufacture_date.Value.Date;
            //}
            return await base.InsertAsync(mobilePropertyEquip);
        }

        public override async Task<PropertyEquipmentdto> UpdateAsync(string mobilepropertyequipId, Delta<PropertyEquipmentdto> patch)
        {
            int propertyequipId = GetKey<int>(mobilepropertyequipId);

            tbl_property_equip existingPropertyEquip = await this.Context.Set<tbl_property_equip>().FindAsync(propertyequipId);
            if (existingPropertyEquip == null)
            {
                throw new HttpResponseException(this.Request.CreateNotFoundResponse());
            }

            PropertyEquipmentdto existingPropertyEquipDTO = Mapper.Map<tbl_property_equip, PropertyEquipmentdto>(existingPropertyEquip);
            patch.Patch(existingPropertyEquipDTO);
            Mapper.Map<PropertyEquipmentdto, tbl_property_equip>(existingPropertyEquipDTO, existingPropertyEquip);

            //if (existingPropertyEquip.property_equip_manufacture_date.HasValue)
            //{
            //    existingPropertyEquip.property_equip_manufacture_date = existingPropertyEquip.property_equip_manufacture_date;
            //}

            await this.SubmitChangesAsync();

            PropertyEquipmentdto updatedPropertyEquipDTO = Mapper.Map<tbl_property_equip, PropertyEquipmentdto>(existingPropertyEquip);

            return updatedPropertyEquipDTO;
        }

        public override async Task<PropertyEquipmentdto> ReplaceAsync(string mobilepropertyequipId, PropertyEquipmentdto mobilePropertyEquip)
        {
            return await base.ReplaceAsync(mobilepropertyequipId, mobilePropertyEquip);
        }

        public override async Task<bool> DeleteAsync(string mobilepropertyequipId)
        {
            int propertyequipId = GetKey<int>(mobilepropertyequipId);
            return await DeleteItemAsync(propertyequipId);
        }
    }
}