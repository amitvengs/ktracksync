﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Web.Http.OData;
using AutoMapper;
using System.Data.Entity;

namespace kTrackMobileApp.Models
{
    public class ValidationStatusDomainManager : MappedEntityDomainManager<ValidationStatusdto, tbl_validation_status>
    {
        private kTrackContext context;

        public ValidationStatusDomainManager(kTrackContext context, HttpRequestMessage request)
            : base(context, request, true)
        {
            Request = request;
            this.context = context;
        }

        public static int GetKey(string mobilevalidationstatusId, DbSet<tbl_validation_status> validationstatus, HttpRequestMessage request)
        {
            int validationstatusId = validationstatus
               .Where(c => c.Id == mobilevalidationstatusId)
               .Select(c => c.validation_status_id)
               .FirstOrDefault();

            if (validationstatusId == 0)
            {
                throw new HttpResponseException(request.CreateNotFoundResponse());
            }
            return validationstatusId;
        }

        protected override T GetKey<T>(string mobilevalidationstatusId)
        {
            return (T)(object)GetKey(mobilevalidationstatusId, this.context.tbl_validation_status, this.Request);
        }

        public override SingleResult<ValidationStatusdto> Lookup(string mobilevalidationstatusId)
        {
            int validationstatusId = GetKey<int>(mobilevalidationstatusId);
            return LookupEntity(c => c.validation_status_id == validationstatusId);
        }

        public override async Task<ValidationStatusdto> InsertAsync(ValidationStatusdto mobilevalidationstatus)
        {
            return await base.InsertAsync(mobilevalidationstatus);
        }

        public override async Task<ValidationStatusdto> UpdateAsync(string mobilevalidationstatusId, Delta<ValidationStatusdto> patch)
        {
            int validationstatusId = GetKey<int>(mobilevalidationstatusId);

            tbl_validation_status existingvalidationstatus = await this.Context.Set<tbl_validation_status>().FindAsync(validationstatusId);
            if (existingvalidationstatus == null)
            {
                throw new HttpResponseException(this.Request.CreateNotFoundResponse());
            }

            ValidationStatusdto existingvalidationstatusDTO = Mapper.Map<tbl_validation_status, ValidationStatusdto>(existingvalidationstatus);
            patch.Patch(existingvalidationstatusDTO);
            Mapper.Map<ValidationStatusdto, tbl_validation_status>(existingvalidationstatusDTO, existingvalidationstatus);

            await this.SubmitChangesAsync();

            ValidationStatusdto updatedvalidationstatusDTO = Mapper.Map<tbl_validation_status, ValidationStatusdto>(existingvalidationstatus);

            return updatedvalidationstatusDTO;
        }

        public override async Task<ValidationStatusdto> ReplaceAsync(string mobilevalidationstatusId, ValidationStatusdto mobilevalidationstatus)
        {
            return await base.ReplaceAsync(mobilevalidationstatusId, mobilevalidationstatus);
        }

        public override async Task<bool> DeleteAsync(string mobilevalidationstatusId)
        {
            int validationstatusId = GetKey<int>(mobilevalidationstatusId);
            return await DeleteItemAsync(validationstatusId);
        }

    }
}