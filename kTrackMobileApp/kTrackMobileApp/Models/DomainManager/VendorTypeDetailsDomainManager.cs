﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using kTrackMobileApp.DataObjects;
using Microsoft.Azure.Mobile.Server;
using System.Net.Http;
using System.Data.Entity;
using System.Web.Http;
using System.Threading.Tasks;
using AutoMapper;
using System.Web.Http.OData;

namespace kTrackMobileApp.Models
{
    public class VendorTypeDetailsDomainManager : MappedEntityDomainManager<VendorTypeDetailsdto, tbl_vendor_type_details>
    {
        private kTrackContext context;

        public VendorTypeDetailsDomainManager(kTrackContext context, HttpRequestMessage request)
            : base(context, request, true)
        {
            Request = request;
            this.context = context;
        }

        public static int GetKey(string mobilevendortypedetailsId, DbSet<tbl_vendor_type_details> validationstatusworkflowdetails, HttpRequestMessage request)
        {
            int vendortypedetailsId = validationstatusworkflowdetails
               .Where(c => c.Id == mobilevendortypedetailsId)
               .Select(c => c.vendor_type_detail_id)
               .FirstOrDefault();

            if (vendortypedetailsId == 0)
            {
                throw new HttpResponseException(request.CreateNotFoundResponse());
            }
            return vendortypedetailsId;
        }

        protected override T GetKey<T>(string mobilevendortypedetailsId)
        {
            return (T)(object)GetKey(mobilevendortypedetailsId, this.context.tbl_vendor_type_details, this.Request);
        }

        public override SingleResult<VendorTypeDetailsdto> Lookup(string mobilevendortypedetailsId)
        {
            int vendortypedetailsId = GetKey<int>(mobilevendortypedetailsId);
            return LookupEntity(c => c.vendor_type_detail_id == vendortypedetailsId);
        }

        private async Task<tbl_vendor_type> VerifyVendorType(string mobileVendorTypeId)
        {
            int vendortypeId = VendorTypeDomainManager.GetKey(mobileVendorTypeId, this.context.tbl_vendor_type, this.Request);
            tbl_vendor_type vendortype = await this.context.tbl_vendor_type.FindAsync(vendortypeId);

            if (vendortype == null)
            {
                throw new HttpResponseException(Request.CreateBadRequestResponse("Vendor Type ID= '{0}' was not found", mobileVendorTypeId));
            }
            return vendortype;
        }
        
        public override async Task<VendorTypeDetailsdto> InsertAsync(VendorTypeDetailsdto mobileVendorTypeDetails)
        {
            tbl_vendor_type vendortype = await VerifyVendorType(mobileVendorTypeDetails.MobileVendorTypeId);
            mobileVendorTypeDetails.vendor_type_id = vendortype.vendor_type_id;

            return await base.InsertAsync(mobileVendorTypeDetails);
        }

        public override async Task<VendorTypeDetailsdto> UpdateAsync(string mobilevendortypedetailsId, Delta<VendorTypeDetailsdto> patch)
        {
            int vendortypedetailsId = GetKey<int>(mobilevendortypedetailsId);
            tbl_vendor_type vendortype = await VerifyVendorType(patch.GetEntity().MobileVendorTypeId);

            tbl_vendor_type_details existingVendorTypeDetails = await this.Context.Set<tbl_vendor_type_details>().FindAsync(vendortypedetailsId);
            if (existingVendorTypeDetails == null)
            {
                throw new HttpResponseException(this.Request.CreateNotFoundResponse());
            }

            VendorTypeDetailsdto existingVendorTypeDetailsDTO = Mapper.Map<tbl_vendor_type_details, VendorTypeDetailsdto>(existingVendorTypeDetails);
            patch.Patch(existingVendorTypeDetailsDTO);
            Mapper.Map<VendorTypeDetailsdto, tbl_vendor_type_details>(existingVendorTypeDetailsDTO, existingVendorTypeDetails);

            existingVendorTypeDetails.vendor_type_id = vendortype.vendor_type_id;

            await this.SubmitChangesAsync();

            VendorTypeDetailsdto updatedVendorTypeDetailsDTO = Mapper.Map<tbl_vendor_type_details, VendorTypeDetailsdto>(existingVendorTypeDetails);

            return updatedVendorTypeDetailsDTO;
        }

        public override async Task<VendorTypeDetailsdto> ReplaceAsync(string mobilevendortypedetailsId, VendorTypeDetailsdto mobileVendorTypeDetails)
        {
            return await base.ReplaceAsync(mobilevendortypedetailsId, mobileVendorTypeDetails);
        }

        public override async Task<bool> DeleteAsync(string mobilevendortypedetailsId)
        {
            int vendortypedetailsId = GetKey<int>(mobilevendortypedetailsId);
            return await DeleteItemAsync(vendortypedetailsId);
        }
    }
}