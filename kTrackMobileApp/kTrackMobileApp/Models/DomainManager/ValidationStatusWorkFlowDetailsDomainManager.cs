﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using kTrackMobileApp.DataObjects;
using Microsoft.Azure.Mobile.Server;
using System.Net.Http;
using System.Data.Entity;
using System.Web.Http;
using System.Threading.Tasks;
using AutoMapper;
using System.Web.Http.OData;

namespace kTrackMobileApp.Models
{
    public class ValidationStatusWorkFlowDetailsDomainManager : MappedEntityDomainManager<ValidationStatusWorkFlowDetailsdto, tbl_validation_status_work_flow_details>
    {
        private kTrackContext context;

        public ValidationStatusWorkFlowDetailsDomainManager(kTrackContext context, HttpRequestMessage request)
            : base(context, request, true)
        {
            Request = request;
            this.context = context;
        }

        public static int GetKey(string mobilevalidationstatusworkflowdetailsId, DbSet<tbl_validation_status_work_flow_details> validationstatusworkflowdetails, HttpRequestMessage request)
        {
            int validationstatusworkflowdetailsId = validationstatusworkflowdetails
               .Where(c => c.Id == mobilevalidationstatusworkflowdetailsId)
               .Select(c => c.validation_status_work_flow_details_id)
               .FirstOrDefault();

            if (validationstatusworkflowdetailsId == 0)
            {
                throw new HttpResponseException(request.CreateNotFoundResponse());
            }
            return validationstatusworkflowdetailsId;
        }

        protected override T GetKey<T>(string mobilevalidationstatusworkflowdetailsId)
        {
            return (T)(object)GetKey(mobilevalidationstatusworkflowdetailsId, this.context.tbl_validation_status_work_flow_details, this.Request);
        }

        public override SingleResult<ValidationStatusWorkFlowDetailsdto> Lookup(string mobilevalidationstatusworkflowdetailsId)
        {
            int validationstatusworkflowdetailsId = GetKey<int>(mobilevalidationstatusworkflowdetailsId);
            return LookupEntity(c => c.validation_status_work_flow_details_id == validationstatusworkflowdetailsId);
        }

        private async Task<tbl_property_equip_status> VerifyPropertyEquipStatus(string mobilePropertyEquipStatusId)
        {
            int propertyequipstatusId = PropertyEquipStatusDomainManager.GetKey(mobilePropertyEquipStatusId, this.context.tbl_property_equip_status, this.Request);
            tbl_property_equip_status propertyequipstatus = await this.context.tbl_property_equip_status.FindAsync(propertyequipstatusId);

            if (propertyequipstatus == null)
            {
                throw new HttpResponseException(Request.CreateBadRequestResponse("Property Equip Status ID= '{0}' was not found", mobilePropertyEquipStatusId));
            }
            return propertyequipstatus;
        }

        private async Task<tbl_validation_status> VerifyValidationStatus(string mobileValidationStatusId)
        {
            int validationstatusId = ValidationStatusDomainManager.GetKey(mobileValidationStatusId, this.context.tbl_validation_status, this.Request);
            tbl_validation_status validationstatus = await this.context.tbl_validation_status.FindAsync(validationstatusId);

            if (validationstatus == null)
            {
                throw new HttpResponseException(Request.CreateBadRequestResponse("Validation Status ID= '{0}' was not found", mobileValidationStatusId));
            }
            return validationstatus;
        }

        private async Task<tbl_validation_status_work_flow> VerifyValidationStatusWorkFlow(string mobileValidationStatusWorkFlowId)
        {
            int validationstatusworkflowId = ValidationStatusWorkFlowDomainManager.GetKey(mobileValidationStatusWorkFlowId, this.context.tbl_validation_status_work_flow, this.Request);
            tbl_validation_status_work_flow validationstatusworkflow = await this.context.tbl_validation_status_work_flow.FindAsync(validationstatusworkflowId);

            if (validationstatusworkflow == null)
            {
                throw new HttpResponseException(Request.CreateBadRequestResponse("Validation Status Work flow ID= '{0}' was not found", mobileValidationStatusWorkFlowId));
            }
            return validationstatusworkflow;
        }

        public override async Task<ValidationStatusWorkFlowDetailsdto> InsertAsync(ValidationStatusWorkFlowDetailsdto mobileValidatonWorkFlowDetails)
        {
            tbl_property_equip_status propertyequipstatus = await VerifyPropertyEquipStatus(mobileValidatonWorkFlowDetails.MobilePropertyEquipStatusId);
            mobileValidatonWorkFlowDetails.property_equip_status_id = propertyequipstatus.property_equip_status_id;

            tbl_validation_status validationstatus = await VerifyValidationStatus(mobileValidatonWorkFlowDetails.MobileValidationStatusId);
            mobileValidatonWorkFlowDetails.validation_status_id = validationstatus.validation_status_id;

            tbl_validation_status_work_flow validationstatusworkflow = await VerifyValidationStatusWorkFlow(mobileValidatonWorkFlowDetails.MobileValidationStatusWorkFlowId);
            mobileValidatonWorkFlowDetails.validation_status_work_flow_id = validationstatusworkflow.validation_status_work_flow_id;

            return await base.InsertAsync(mobileValidatonWorkFlowDetails);
        }

        public override async Task<ValidationStatusWorkFlowDetailsdto> UpdateAsync(string mobilevalidationstatusworkflowdetailsId, Delta<ValidationStatusWorkFlowDetailsdto> patch)
        {
            int validationstatusworkflowdetailsId = GetKey<int>(mobilevalidationstatusworkflowdetailsId);
            tbl_property_equip_status propertyequipstatus = await VerifyPropertyEquipStatus(patch.GetEntity().MobilePropertyEquipStatusId);
            tbl_validation_status validationstatus = await VerifyValidationStatus(patch.GetEntity().MobileValidationStatusId);
            tbl_validation_status_work_flow validationstatusworkflow = await VerifyValidationStatusWorkFlow(patch.GetEntity().MobileValidationStatusWorkFlowId);

            tbl_validation_status_work_flow_details existingValidatonWorkFlowDetails = await this.Context.Set<tbl_validation_status_work_flow_details>().FindAsync(validationstatusworkflowdetailsId);
            if (existingValidatonWorkFlowDetails == null)
            {
                throw new HttpResponseException(this.Request.CreateNotFoundResponse());
            }

            ValidationStatusWorkFlowDetailsdto existingValidatonWorkFlowDetailsDTO = Mapper.Map<tbl_validation_status_work_flow_details, ValidationStatusWorkFlowDetailsdto>(existingValidatonWorkFlowDetails);
            patch.Patch(existingValidatonWorkFlowDetailsDTO);
            Mapper.Map<ValidationStatusWorkFlowDetailsdto, tbl_validation_status_work_flow_details>(existingValidatonWorkFlowDetailsDTO, existingValidatonWorkFlowDetails);

            existingValidatonWorkFlowDetails.property_equip_status_id = propertyequipstatus.property_equip_status_id;
            existingValidatonWorkFlowDetails.validation_status_id = validationstatus.validation_status_id;
            existingValidatonWorkFlowDetails.validation_status_work_flow_id = validationstatusworkflow.validation_status_work_flow_id;

            await this.SubmitChangesAsync();

            ValidationStatusWorkFlowDetailsdto updatedValidatonWorkFlowDetailsDTO = Mapper.Map<tbl_validation_status_work_flow_details, ValidationStatusWorkFlowDetailsdto>(existingValidatonWorkFlowDetails);

            return updatedValidatonWorkFlowDetailsDTO;
        }

        public override async Task<ValidationStatusWorkFlowDetailsdto> ReplaceAsync(string mobilevalidationstatusworkflowdetailsId, ValidationStatusWorkFlowDetailsdto mobileValidationWorkFlowDetails)
        {
            return await base.ReplaceAsync(mobilevalidationstatusworkflowdetailsId, mobileValidationWorkFlowDetails);
        }

        public override async Task<bool> DeleteAsync(string mobilevalidationstatusworkflowdetailsId)
        {
            int validationstatusworkflowdetailsId = GetKey<int>(mobilevalidationstatusworkflowdetailsId);
            return await DeleteItemAsync(validationstatusworkflowdetailsId);
        }
    }
}