﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Azure.Mobile.Server;
using kTrackMobileApp.DataObjects;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Web.Http.OData;
using AutoMapper;
using System.Data.Entity;

namespace kTrackMobileApp.Models
{
    public class PropertyBarCodeDomainManager : MappedEntityDomainManager<PropertyBarcodedto, tbl_property_barcode>
    {
        private kTrackContext context;

        public PropertyBarCodeDomainManager(kTrackContext context, HttpRequestMessage request)
            : base(context, request, true)
        {
            Request = request;
            this.context = context;
        }

        public static int GetKey(string mobilepropertybarcodeId, DbSet<tbl_property_barcode> propertybarcode, HttpRequestMessage request)
        {
            int propertybarcodeId = propertybarcode
               .Where(c => c.Id == mobilepropertybarcodeId)
               .Select(c => c.barcode_id)
               .FirstOrDefault();

            if (propertybarcodeId == 0)
            {
                throw new HttpResponseException(request.CreateNotFoundResponse());
            }
            return propertybarcodeId;
        }

        protected override T GetKey<T>(string mobilepropertybarcodeId)
        {
            return (T)(object)GetKey(mobilepropertybarcodeId, this.context.tbl_property_barcode, this.Request);
        }

        public override SingleResult<PropertyBarcodedto> Lookup(string mobilepropertybarcodeId)
        {
            int barcodeId = GetKey<int>(mobilepropertybarcodeId);
            return LookupEntity(c => c.barcode_id == barcodeId);
        }

        private async Task<tbl_property> VerifyProperty(string mobilePropertyId)
        {
            int propertyId = PropertyDomainManager.GetKey(mobilePropertyId, this.context.tbl_property, this.Request);
            tbl_property property = await this.context.tbl_property.FindAsync(propertyId);

            if (property == null)
            {
                throw new HttpResponseException(Request.CreateBadRequestResponse("PropertyID= '{0}' was not found", mobilePropertyId));
            }
            return property;
        }

        private async Task<tbl_property_equip> VerifyPropertyEquip(string mobilePropertyEquipId)
        {
            int propertyequipId = PropertyEquipDomainManager.GetKey(mobilePropertyEquipId, this.context.tbl_property_equip, this.Request);
            tbl_property_equip propertyequip = await this.context.tbl_property_equip.FindAsync(propertyequipId);

            if (propertyequip == null)
            {
                throw new HttpResponseException(Request.CreateBadRequestResponse("Property Equip ID= '{0}' was not found", mobilePropertyEquipId));
            }
            return propertyequip;
        }

        public override async Task<PropertyBarcodedto> InsertAsync(PropertyBarcodedto mobilePropertyBarCode)
        {
            tbl_property property = await VerifyProperty(mobilePropertyBarCode.MobilePropertyId);
            mobilePropertyBarCode.property_id = property.property_id;

            tbl_property_equip propertyequip = await VerifyPropertyEquip(mobilePropertyBarCode.MobilePropertyEquipId);
            mobilePropertyBarCode.mapping_id = propertyequip.property_equip_id;

            return await base.InsertAsync(mobilePropertyBarCode);
        }

        public override async Task<PropertyBarcodedto> UpdateAsync(string mobilepropertybarcodeId, Delta<PropertyBarcodedto> patch)
        {
            int barcodeId = GetKey<int>(mobilepropertybarcodeId);
            tbl_property property = await VerifyProperty(patch.GetEntity().MobilePropertyId);
            tbl_property_equip propertyequip = await VerifyPropertyEquip(patch.GetEntity().MobilePropertyEquipId);

            tbl_property_barcode existingBarCode = await this.Context.Set<tbl_property_barcode>().FindAsync(barcodeId);
            if (existingBarCode == null)
            {
                throw new HttpResponseException(this.Request.CreateNotFoundResponse());
            }

            PropertyBarcodedto existingBarCodeDTO = Mapper.Map<tbl_property_barcode, PropertyBarcodedto>(existingBarCode);
            patch.Patch(existingBarCodeDTO);
            Mapper.Map<PropertyBarcodedto, tbl_property_barcode>(existingBarCodeDTO, existingBarCode);

            // This is required to map the right Id for the customer
            existingBarCode.property_id = property.property_id;
            existingBarCode.mapping_id = propertyequip.property_equip_id;

            await this.SubmitChangesAsync();

            PropertyBarcodedto updatedBarCodeDTO = Mapper.Map<tbl_property_barcode, PropertyBarcodedto>(existingBarCode);

            return updatedBarCodeDTO;
        }

        public override async Task<PropertyBarcodedto> ReplaceAsync(string mobilepropertybarcodeId, PropertyBarcodedto mobileBarCode)
        {
            return await base.ReplaceAsync(mobilepropertybarcodeId, mobileBarCode);
        }

        public override async Task<bool> DeleteAsync(string mobilepropertybarcodeId)
        {
            int barcodeId = GetKey<int>(mobilepropertybarcodeId);
            return await DeleteItemAsync(barcodeId);
        }
    }
}