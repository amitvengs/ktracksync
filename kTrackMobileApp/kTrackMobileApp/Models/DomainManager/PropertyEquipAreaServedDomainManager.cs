﻿using Microsoft.Azure.Mobile.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using kTrackMobileApp.DataObjects;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Web.Http.OData;
using AutoMapper;

namespace kTrackMobileApp.Models
{
    public class PropertyEquipAreaServedDomainManager : MappedEntityDomainManager<PropertyEquipmentAreaServeddto, tbl_property_equip_area_served>
    {
        private kTrackContext context;

        public PropertyEquipAreaServedDomainManager(kTrackContext context, HttpRequestMessage request)
            : base(context, request, true)
        {
            Request = request;
            this.context = context;
        }

        protected override T GetKey<T>(string mobilepropertyareaservedId)
        {
            int propertyareaservedId = this.context.tbl_property_equip_area_served
                .Where(p => p.Id == mobilepropertyareaservedId)
                .Select(p => p.property_equip_area_served_id)
                .FirstOrDefault();

            if (propertyareaservedId == 0)
            {
                throw new HttpResponseException(this.Request.CreateNotFoundResponse());
            }
            return (T)(object)propertyareaservedId;
        }

        public override SingleResult<PropertyEquipmentAreaServeddto> Lookup(string mobilepropertyareaservedId)
        {
            int propertyareaservedId = GetKey<int>(mobilepropertyareaservedId);
            return LookupEntity(p => p.property_equip_area_served_id == propertyareaservedId);
        }

        private async Task<tbl_property_equip> VerifyPropertyEquip(string mobilePropertyEquipId)
        {
            int propertyequipId = PropertyEquipDomainManager.GetKey(mobilePropertyEquipId, this.context.tbl_property_equip, this.Request);
            tbl_property_equip propertyequip = await this.context.tbl_property_equip.FindAsync(propertyequipId);

            if (propertyequip == null)
            {
                throw new HttpResponseException(Request.CreateBadRequestResponse("Property Equip ID= '{0}' was not found", mobilePropertyEquipId));
            }
            return propertyequip;
        }

        private async Task<tbl_property_loc> VerifyPropertyLoc(string mobilePropertyLocId)
        {
            int propertylocId = PropertyLocDomainManager.GetKey(mobilePropertyLocId, this.context.tbl_property_loc, this.Request);
            tbl_property_loc propertyloc = await this.context.tbl_property_loc.FindAsync(propertylocId);

            if (propertyloc == null)
            {
                throw new HttpResponseException(Request.CreateBadRequestResponse("Property Loc ID= '{0}' was not found", mobilePropertyLocId));
            }
            return propertyloc;
        }

        public override async Task<PropertyEquipmentAreaServeddto> InsertAsync(PropertyEquipmentAreaServeddto mobilePropertyEquipAreaServed)
        {
            tbl_property_equip propertyequip = await VerifyPropertyEquip(mobilePropertyEquipAreaServed.MobilePropertyEquipId);
            mobilePropertyEquipAreaServed.property_equip_id = propertyequip.property_equip_id;

            tbl_property_loc propertyloc = await VerifyPropertyLoc(mobilePropertyEquipAreaServed.MobilePropertyLocId);
            mobilePropertyEquipAreaServed.property_loc_id = propertyloc.property_loc_id;

            return await base.InsertAsync(mobilePropertyEquipAreaServed);
        }

        public override async Task<PropertyEquipmentAreaServeddto> UpdateAsync(string mobilepropertyareaservedId, Delta<PropertyEquipmentAreaServeddto> patch)
        {
            tbl_property_equip propertyequip = await VerifyPropertyEquip(patch.GetEntity().MobilePropertyEquipId);
            tbl_property_loc propertyloc = await VerifyPropertyLoc(patch.GetEntity().MobilePropertyLocId);

            int propertyareaservedId = GetKey<int>(mobilepropertyareaservedId);

            tbl_property_equip_area_served existingPropertyEquipAreaServed = await this.Context.Set<tbl_property_equip_area_served>().FindAsync(propertyareaservedId);
            if (existingPropertyEquipAreaServed == null)
            {
                throw new HttpResponseException(this.Request.CreateNotFoundResponse());
            }

            PropertyEquipmentAreaServeddto existingPropertyEquipAreaServedDTO = Mapper.Map<tbl_property_equip_area_served, PropertyEquipmentAreaServeddto>(existingPropertyEquipAreaServed);
            patch.Patch(existingPropertyEquipAreaServedDTO);
            Mapper.Map<PropertyEquipmentAreaServeddto, tbl_property_equip_area_served>(existingPropertyEquipAreaServedDTO, existingPropertyEquipAreaServed);

            // This is required to map the right Id for the customer
            existingPropertyEquipAreaServed.property_equip_id = propertyequip.property_equip_id;
            existingPropertyEquipAreaServed.property_loc_id = propertyloc.property_loc_id;

            await this.SubmitChangesAsync();

            PropertyEquipmentAreaServeddto updatedPropertyEquipAreaServedDTO = Mapper.Map<tbl_property_equip_area_served, PropertyEquipmentAreaServeddto>(existingPropertyEquipAreaServed);

            return updatedPropertyEquipAreaServedDTO;
        }

        public override async Task<PropertyEquipmentAreaServeddto> ReplaceAsync(string mobilepropertyareaservedId, PropertyEquipmentAreaServeddto mobilePropertyEquipAreaServed)
        {
            await VerifyPropertyEquip(mobilePropertyEquipAreaServed.MobilePropertyEquipId);

            return await base.ReplaceAsync(mobilepropertyareaservedId, mobilePropertyEquipAreaServed);
        }

        public override Task<bool> DeleteAsync(string mobilepropertyareaservedId)
        {
            int propertyareaservedId = GetKey<int>(mobilepropertyareaservedId);
            return DeleteItemAsync(propertyareaservedId);
        }
    }
}