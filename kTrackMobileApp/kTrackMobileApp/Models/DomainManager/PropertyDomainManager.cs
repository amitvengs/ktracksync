﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using kTrackMobileApp.DataObjects;
using Microsoft.Azure.Mobile.Server;
using System.Net.Http;
using System.Data.Entity;
using System.Web.Http;
using System.Threading.Tasks;
using AutoMapper;
using System.Web.Http.OData;

namespace kTrackMobileApp.Models
{
    public class PropertyDomainManager : MappedEntityDomainManager<Propertydto, tbl_property>
    {
        private kTrackContext context;

        public PropertyDomainManager(kTrackContext context, HttpRequestMessage request)
            : base(context, request, true)
        {
            Request = request;
            this.context = context;
        }

        public static int GetKey(string mobilepropertyId, DbSet<tbl_property> property, HttpRequestMessage request)
        {
            int propertyId = property
               .Where(c => c.Id == mobilepropertyId)
               .Select(c => c.property_id)
               .FirstOrDefault();

            if (propertyId == 0)
            {
                throw new HttpResponseException(request.CreateNotFoundResponse());
            }
            return propertyId;
        }

        protected override T GetKey<T>(string mobilepropertyId)
        {
            return (T)(object)GetKey(mobilepropertyId, this.context.tbl_property, this.Request);
        }

        public override SingleResult<Propertydto> Lookup(string mobilepropertyId)
        {
            int propertyId = GetKey<int>(mobilepropertyId);
            return LookupEntity(c => c.property_id == propertyId);
        }
        
        public override async Task<Propertydto> InsertAsync(Propertydto mobileProperty)
        {
            return await base.InsertAsync(mobileProperty);
        }

        public override async Task<Propertydto> UpdateAsync(string mobilepropertyId, Delta<Propertydto> patch)
        {
            int propertyId = GetKey<int>(mobilepropertyId);

            tbl_property existingProperty = await this.Context.Set<tbl_property>().FindAsync(propertyId);
            if (existingProperty == null)
            {
                throw new HttpResponseException(this.Request.CreateNotFoundResponse());
            }

            Propertydto existingPropertyDTO = Mapper.Map<tbl_property, Propertydto>(existingProperty);
            patch.Patch(existingPropertyDTO);
            Mapper.Map<Propertydto, tbl_property>(existingPropertyDTO, existingProperty);

            await this.SubmitChangesAsync();

            Propertydto updatedPropertypDTO = Mapper.Map<tbl_property, Propertydto>(existingProperty);

            return updatedPropertypDTO;
        }

        public override async Task<Propertydto> ReplaceAsync(string mobilepropertyId, Propertydto mobileProperty)
        {
            return await base.ReplaceAsync(mobilepropertyId, mobileProperty);
        }

        public override async Task<bool> DeleteAsync(string mobilepropertyId)
        {
            int propertyId = GetKey<int>(mobilepropertyId);
            return await DeleteItemAsync(propertyId);
        }
    }
}