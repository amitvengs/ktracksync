﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using kTrackMobileApp.DataObjects;
using Microsoft.Azure.Mobile.Server;
using System.Net.Http;
using System.Data.Entity;
using System.Web.Http;
using System.Threading.Tasks;
using AutoMapper;
using System.Web.Http.OData;

namespace kTrackMobileApp.Models
{
    public class PropertyLocDomainManager : MappedEntityDomainManager<PropertyLocationdto, tbl_property_loc>
    {
        private kTrackContext context;

        public PropertyLocDomainManager(kTrackContext context, HttpRequestMessage request)
            : base(context, request, true)
        {
            Request = request;
            this.context = context;
        }

        public static int GetKey(string mobilepropertylocId, DbSet<tbl_property_loc> propertyloc, HttpRequestMessage request)
        {
            int propertylocId = propertyloc
               .Where(c => c.Id == mobilepropertylocId)
               .Select(c => c.property_loc_id)
               .FirstOrDefault();

            if (propertylocId == 0)
            {
                throw new HttpResponseException(request.CreateNotFoundResponse());
            }
            return propertylocId;
        }

        protected override T GetKey<T>(string mobilepropertylocId)
        {
            return (T)(object)GetKey(mobilepropertylocId, this.context.tbl_property_loc, this.Request);
        }

        public override SingleResult<PropertyLocationdto> Lookup(string mobilepropertylocId)
        {
            int propertylocId = GetKey<int>(mobilepropertylocId);
            return LookupEntity(c => c.property_loc_id == propertylocId);
        }

        public override async Task<PropertyLocationdto> InsertAsync(PropertyLocationdto mobilePropertyLoc)
        {
            return await base.InsertAsync(mobilePropertyLoc);
        }

        public override async Task<PropertyLocationdto> UpdateAsync(string mobilepropertylocId, Delta<PropertyLocationdto> patch)
        {
            int propertylocId = GetKey<int>(mobilepropertylocId);

            tbl_property_loc existingPropertyLoc = await this.Context.Set<tbl_property_loc>().FindAsync(propertylocId);
            if (existingPropertyLoc == null)
            {
                throw new HttpResponseException(this.Request.CreateNotFoundResponse());
            }

            PropertyLocationdto existingPropertyLocDTO = Mapper.Map<tbl_property_loc, PropertyLocationdto>(existingPropertyLoc);
            patch.Patch(existingPropertyLocDTO);
            Mapper.Map<PropertyLocationdto, tbl_property_loc>(existingPropertyLocDTO, existingPropertyLoc);

            await this.SubmitChangesAsync();

            PropertyLocationdto updatedPropertyEquipDTO = Mapper.Map<tbl_property_loc, PropertyLocationdto>(existingPropertyLoc);

            return updatedPropertyEquipDTO;
        }

        public override async Task<PropertyLocationdto> ReplaceAsync(string mobilepropertylocId, PropertyLocationdto mobilePropertyLoc)
        {
            return await base.ReplaceAsync(mobilepropertylocId, mobilePropertyLoc);
        }

        public override async Task<bool> DeleteAsync(string mobilepropertylocId)
        {
            int propertylocId = GetKey<int>(mobilepropertylocId);
            return await DeleteItemAsync(propertylocId);
        }
    }
}