﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using kTrackMobileApp.DataObjects;
using Microsoft.Azure.Mobile.Server;
using System.Net.Http;
using System.Data.Entity;
using System.Web.Http;
using System.Threading.Tasks;
using AutoMapper;
using System.Web.Http.OData;

namespace kTrackMobileApp.Models
{
    public class ValidationStatusWorkFlowDomainManager : MappedEntityDomainManager<ValidationStatusWorkFlowdto, tbl_validation_status_work_flow>
    {
        private kTrackContext context;

        public ValidationStatusWorkFlowDomainManager(kTrackContext context, HttpRequestMessage request)
            : base(context, request, true)
        {
            Request = request;
            this.context = context;
        }

        public static int GetKey(string mobilevalidationstatusworkflowId, DbSet<tbl_validation_status_work_flow> propertyequip, HttpRequestMessage request)
        {
            int validationstatusworkflowId = propertyequip
               .Where(c => c.Id == mobilevalidationstatusworkflowId)
               .Select(c => c.validation_status_work_flow_id)
               .FirstOrDefault();

            if (validationstatusworkflowId == 0)
            {
                throw new HttpResponseException(request.CreateNotFoundResponse());
            }
            return validationstatusworkflowId;
        }

        protected override T GetKey<T>(string mobilevalidationstatusworkflowId)
        {
            return (T)(object)GetKey(mobilevalidationstatusworkflowId, this.context.tbl_validation_status_work_flow, this.Request);
        }

        public override SingleResult<ValidationStatusWorkFlowdto> Lookup(string mobilevalidationstatusworkflowId)
        {
            int validationstatusworkflowId = GetKey<int>(mobilevalidationstatusworkflowId);
            return LookupEntity(c => c.validation_status_work_flow_id == validationstatusworkflowId);
        }

        public override async Task<ValidationStatusWorkFlowdto> InsertAsync(ValidationStatusWorkFlowdto mobileValidationWorkFlow)
        {
            return await base.InsertAsync(mobileValidationWorkFlow);
        }

        public override async Task<ValidationStatusWorkFlowdto> UpdateAsync(string mobilevalidationstatusworkflowId, Delta<ValidationStatusWorkFlowdto> patch)
        {
            int validationstatusworkflowId = GetKey<int>(mobilevalidationstatusworkflowId);

            tbl_validation_status_work_flow existingValidationWorkFlow = await this.Context.Set<tbl_validation_status_work_flow>().FindAsync(validationstatusworkflowId);
            if (existingValidationWorkFlow == null)
            {
                throw new HttpResponseException(this.Request.CreateNotFoundResponse());
            }

            ValidationStatusWorkFlowdto existingValidationWorkFlowDTO = Mapper.Map<tbl_validation_status_work_flow, ValidationStatusWorkFlowdto>(existingValidationWorkFlow);
            patch.Patch(existingValidationWorkFlowDTO);
            Mapper.Map<ValidationStatusWorkFlowdto, tbl_validation_status_work_flow>(existingValidationWorkFlowDTO, existingValidationWorkFlow);

            await this.SubmitChangesAsync();

            ValidationStatusWorkFlowdto updatedValidationWorkFlowDTO = Mapper.Map<tbl_validation_status_work_flow, ValidationStatusWorkFlowdto>(existingValidationWorkFlow);

            return updatedValidationWorkFlowDTO;
        }

        public override async Task<ValidationStatusWorkFlowdto> ReplaceAsync(string mobilevalidationstatusworkflowId, ValidationStatusWorkFlowdto mobileValidationWorkFlow)
        {
            return await base.ReplaceAsync(mobilevalidationstatusworkflowId, mobileValidationWorkFlow);
        }

        public override async Task<bool> DeleteAsync(string mobilevalidationstatusworkflowId)
        {
            int validationstatusworkflowId = GetKey<int>(mobilevalidationstatusworkflowId);
            return await DeleteItemAsync(validationstatusworkflowId);
        }
    }
}