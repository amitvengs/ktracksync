﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using kTrackMobileApp.DataObjects;
using Microsoft.Azure.Mobile.Server;
using System.Net.Http;
using System.Data.Entity;
using System.Web.Http;
using System.Threading.Tasks;
using AutoMapper;
using System.Web.Http.OData;

namespace kTrackMobileApp.Models
{
    public class VendorTypeDomainManager : MappedEntityDomainManager<VendorTypedto, tbl_vendor_type>
    {
        private kTrackContext context;

        public VendorTypeDomainManager(kTrackContext context, HttpRequestMessage request)
            : base(context, request, true)
        {
            Request = request;
            this.context = context;
        }

        public static int GetKey(string mobilevendortypeId, DbSet<tbl_vendor_type> propertyequip, HttpRequestMessage request)
        {
            int vendortypeId = propertyequip
               .Where(c => c.Id == mobilevendortypeId)
               .Select(c => c.vendor_type_id)
               .FirstOrDefault();

            if (vendortypeId == 0)
            {
                throw new HttpResponseException(request.CreateNotFoundResponse());
            }
            return vendortypeId;
        }

        protected override T GetKey<T>(string mobilevendortypeId)
        {
            return (T)(object)GetKey(mobilevendortypeId, this.context.tbl_vendor_type, this.Request);
        }

        public override SingleResult<VendorTypedto> Lookup(string mobilevendortypeId)
        {
            int vendortypeId = GetKey<int>(mobilevendortypeId);
            return LookupEntity(c => c.vendor_type_id == vendortypeId);
        }

        public override async Task<VendorTypedto> InsertAsync(VendorTypedto mobileValidationWorkFlow)
        {
            return await base.InsertAsync(mobileValidationWorkFlow);
        }

        public override async Task<VendorTypedto> UpdateAsync(string mobilevendortypeId, Delta<VendorTypedto> patch)
        {
            int vendortypeId = GetKey<int>(mobilevendortypeId);

            tbl_vendor_type existingVendorType = await this.Context.Set<tbl_vendor_type>().FindAsync(vendortypeId);
            if (existingVendorType == null)
            {
                throw new HttpResponseException(this.Request.CreateNotFoundResponse());
            }

            VendorTypedto existingVendorTypeDTO = Mapper.Map<tbl_vendor_type, VendorTypedto>(existingVendorType);
            patch.Patch(existingVendorTypeDTO);
            Mapper.Map<VendorTypedto, tbl_vendor_type>(existingVendorTypeDTO, existingVendorType);

            await this.SubmitChangesAsync();

            VendorTypedto updatedVendorTypeDTO = Mapper.Map<tbl_vendor_type, VendorTypedto>(existingVendorType);

            return updatedVendorTypeDTO;
        }

        public override async Task<VendorTypedto> ReplaceAsync(string mobilevendortypeId, VendorTypedto mobileVendorType)
        {
            return await base.ReplaceAsync(mobilevendortypeId, mobileVendorType);
        }

        public override async Task<bool> DeleteAsync(string mobilevendortypeId)
        {
            int vendortypeId = GetKey<int>(mobilevendortypeId);
            return await DeleteItemAsync(vendortypeId);
        }
    }
}