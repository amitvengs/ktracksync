﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using kTrackMobileApp.DataObjects;
using Microsoft.Azure.Mobile.Server;
using System.Net.Http;
using System.Data.Entity;
using System.Web.Http;
using System.Threading.Tasks;
using AutoMapper;
using System.Web.Http.OData;

namespace kTrackMobileApp.Models
{
    public class PropertyEquipValidationStatusDetailsDomainManager : MappedEntityDomainManager<PropertyEquipValidationStatusDetailsdto, tbl_property_equip_validation_status_details>
    {
        private kTrackContext context;

        public PropertyEquipValidationStatusDetailsDomainManager(kTrackContext context, HttpRequestMessage request)
            : base(context, request, true)
        {
            Request = request;
            this.context = context;
        }

        public static int GetKey(string mobilepropertyvalidationstatusId, DbSet<tbl_property_equip_validation_status_details> propertyvalidationstatus, HttpRequestMessage request)
        {
            int propertyvalidationstatusId = propertyvalidationstatus
               .Where(c => c.Id == mobilepropertyvalidationstatusId)
               .Select(c => c.property_equip_validation_status_details_id)
               .FirstOrDefault();

            if (propertyvalidationstatusId == 0)
            {
                throw new HttpResponseException(request.CreateNotFoundResponse());
            }
            return propertyvalidationstatusId;
        }

        protected override T GetKey<T>(string mobilepropertyvalidationstatusId)
        {
            return (T)(object)GetKey(mobilepropertyvalidationstatusId, this.context.tbl_property_equip_validation_status_details, this.Request);
        }

        public override SingleResult<PropertyEquipValidationStatusDetailsdto> Lookup(string mobilepropertyvalidationstatusId)
        {
            int propertyvalidationstatusId = GetKey<int>(mobilepropertyvalidationstatusId);
            return LookupEntity(c => c.property_equip_validation_status_details_id == propertyvalidationstatusId);
        }

        private async Task<tbl_property_equip> VerifyPropertyEquip(string mobilePropertyEquipId)
        {
            int propertyequipId = PropertyEquipDomainManager.GetKey(mobilePropertyEquipId, this.context.tbl_property_equip, this.Request);
            tbl_property_equip propertyequip = await this.context.tbl_property_equip.FindAsync(propertyequipId);

            if (propertyequip == null)
            {
                throw new HttpResponseException(Request.CreateBadRequestResponse("Property Equip ID= '{0}' was not found", mobilePropertyEquipId));
            }
            return propertyequip;
        }

        private async Task<tbl_validation_status> VerifyValidationStatus(string mobileValidationStatusId)
        {
            int validationstatusId = ValidationStatusDomainManager.GetKey(mobileValidationStatusId, this.context.tbl_validation_status, this.Request);
            tbl_validation_status validationstatus = await this.context.tbl_validation_status.FindAsync(validationstatusId);

            if (validationstatus == null)
            {
                throw new HttpResponseException(Request.CreateBadRequestResponse("Validation Status ID= '{0}' was not found", mobileValidationStatusId));
            }
            return validationstatus;
        }

        public override async Task<PropertyEquipValidationStatusDetailsdto> InsertAsync(PropertyEquipValidationStatusDetailsdto mobilePropertyValidationStatus)
        {
            tbl_property_equip propertyequip = await VerifyPropertyEquip(mobilePropertyValidationStatus.MobilePropertyEquipId);
            mobilePropertyValidationStatus.property_equip_id = propertyequip.property_equip_id;

            tbl_validation_status validationstatus = await VerifyValidationStatus(mobilePropertyValidationStatus.MobileValidationStatusId);
            mobilePropertyValidationStatus.validation_status_id = validationstatus.validation_status_id;

            return await base.InsertAsync(mobilePropertyValidationStatus);
        }

        public override async Task<PropertyEquipValidationStatusDetailsdto> UpdateAsync(string mobilepropertyvalidationstatusId, Delta<PropertyEquipValidationStatusDetailsdto> patch)
        {
            int propertyvalidatoinstatusId = GetKey<int>(mobilepropertyvalidationstatusId);
            tbl_property_equip propertyequip = await VerifyPropertyEquip(patch.GetEntity().MobilePropertyEquipId);
            tbl_validation_status validationstatus = await VerifyValidationStatus(patch.GetEntity().MobileValidationStatusId);

            tbl_property_equip_validation_status_details existingPropertyValidationStatus = await this.Context.Set<tbl_property_equip_validation_status_details>().FindAsync(propertyvalidatoinstatusId);
            if (existingPropertyValidationStatus == null)
            {
                throw new HttpResponseException(this.Request.CreateNotFoundResponse());
            }

            PropertyEquipValidationStatusDetailsdto existingPropertyValidationStatusDTO = Mapper.Map<tbl_property_equip_validation_status_details, PropertyEquipValidationStatusDetailsdto>(existingPropertyValidationStatus);
            patch.Patch(existingPropertyValidationStatusDTO);
            Mapper.Map<PropertyEquipValidationStatusDetailsdto, tbl_property_equip_validation_status_details>(existingPropertyValidationStatusDTO, existingPropertyValidationStatus);

            // This is required to map the right Id for the customer
            existingPropertyValidationStatus.property_equip_id = propertyequip.property_equip_id;
            existingPropertyValidationStatus.validation_status_id = validationstatus.validation_status_id;

            await this.SubmitChangesAsync();

            PropertyEquipValidationStatusDetailsdto updatedPropertyValidationStatusDTO = Mapper.Map<tbl_property_equip_validation_status_details, PropertyEquipValidationStatusDetailsdto>(existingPropertyValidationStatus);

            return updatedPropertyValidationStatusDTO;
        }

        public override async Task<PropertyEquipValidationStatusDetailsdto> ReplaceAsync(string mobilepropertyvalidationstatusId, PropertyEquipValidationStatusDetailsdto mobileBarCode)
        {
            return await base.ReplaceAsync(mobilepropertyvalidationstatusId, mobileBarCode);
        }

        public override async Task<bool> DeleteAsync(string mobilepropertyvalidationstatusId)
        {
            int propertyvalidatoinstatusId = GetKey<int>(mobilepropertyvalidationstatusId);
            return await DeleteItemAsync(propertyvalidatoinstatusId);
        }
    }
}