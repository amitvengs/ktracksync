﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using kTrackMobileApp.DataObjects;
using Microsoft.Azure.Mobile.Server;
using System.Net.Http;
using System.Data.Entity;
using System.Web.Http;
using System.Threading.Tasks;
using AutoMapper;
using System.Web.Http.OData;

namespace kTrackMobileApp.Models
{
    public class PropertyEquipStatusDomainManager : MappedEntityDomainManager<PropertyEquipmentStatusdto, tbl_property_equip_status>
    {
        private kTrackContext context;

        public PropertyEquipStatusDomainManager(kTrackContext context, HttpRequestMessage request)
            : base(context, request, true)
        {
            Request = request;
            this.context = context;
        }

        public static int GetKey(string mobilepropertyequipstatusId, DbSet<tbl_property_equip_status> propertyequipstatus, HttpRequestMessage request)
        {
            int propertyequipstatusId = propertyequipstatus
               .Where(c => c.Id == mobilepropertyequipstatusId)
               .Select(c => c.property_equip_status_id)
               .FirstOrDefault();

            if (propertyequipstatusId == 0)
            {
                throw new HttpResponseException(request.CreateNotFoundResponse());
            }
            return propertyequipstatusId;
        }

        protected override T GetKey<T>(string mobilepropertyequipstatusId)
        {
            return (T)(object)GetKey(mobilepropertyequipstatusId, this.context.tbl_property_equip_status, this.Request);
        }

        public override SingleResult<PropertyEquipmentStatusdto> Lookup(string mobilepropertyequipstatusId)
        {
            int propertyequipstatusId = GetKey<int>(mobilepropertyequipstatusId);
            return LookupEntity(c => c.property_equip_status_id == propertyequipstatusId);
        }

        public override async Task<PropertyEquipmentStatusdto> InsertAsync(PropertyEquipmentStatusdto mobilePropertyEquipStatus)
        {
            return await base.InsertAsync(mobilePropertyEquipStatus);
        }

        public override async Task<PropertyEquipmentStatusdto> UpdateAsync(string mobilepropertyequipstatusId, Delta<PropertyEquipmentStatusdto> patch)
        {
            int propertyequipstatusId = GetKey<int>(mobilepropertyequipstatusId);

            tbl_property_equip_status existingPropertyEquipStatus = await this.Context.Set<tbl_property_equip_status>().FindAsync(propertyequipstatusId);
            if (existingPropertyEquipStatus == null)
            {
                throw new HttpResponseException(this.Request.CreateNotFoundResponse());
            }

            PropertyEquipmentStatusdto existingPropertyEquipStatusDTO = Mapper.Map<tbl_property_equip_status, PropertyEquipmentStatusdto>(existingPropertyEquipStatus);
            patch.Patch(existingPropertyEquipStatusDTO);
            Mapper.Map<PropertyEquipmentStatusdto, tbl_property_equip_status>(existingPropertyEquipStatusDTO, existingPropertyEquipStatus);

            await this.SubmitChangesAsync();

            PropertyEquipmentStatusdto updatedPropertyEquipStatusDTO = Mapper.Map<tbl_property_equip_status, PropertyEquipmentStatusdto>(existingPropertyEquipStatus);

            return updatedPropertyEquipStatusDTO;
        }

        public override async Task<PropertyEquipmentStatusdto> ReplaceAsync(string mobilepropertyequipstatusId, PropertyEquipmentStatusdto mobileValidationWorkFlow)
        {
            return await base.ReplaceAsync(mobilepropertyequipstatusId, mobileValidationWorkFlow);
        }

        public override async Task<bool> DeleteAsync(string mobilepropertyequipstatusId)
        {
            int propertyequipstatusId = GetKey<int>(mobilepropertyequipstatusId);
            return await DeleteItemAsync(propertyequipstatusId);
        }
    }
}