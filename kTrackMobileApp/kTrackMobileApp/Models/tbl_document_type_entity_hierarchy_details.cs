namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_document_type_entity_hierarchy_details
    {
        [Key]
        public int document_type_entity_hierarchy_details_id { get; set; }

        public int entity_id { get; set; }

        public int document_type_hierarchy_id { get; set; }

        public bool is_enabled { get; set; }

        public int created_by_user_id { get; set; }

        public DateTime creation_date { get; set; }

        public int? document_info_id_temp { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public bool Deleted { get; set; }
    }
}
