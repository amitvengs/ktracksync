namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_property_project_user_access_details
    {
        [Key]
        public int property_project_user_access_details_id { get; set; }

        public int property_project_id { get; set; }

        public int user_id { get; set; }

        public bool is_active { get; set; }

        public int created_by_user_id { get; set; }

        public DateTime creation_date { get; set; }

        public Guid rowguid { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public bool Deleted { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public virtual tbl_um_users tbl_um_users { get; set; }

        public virtual tbl_um_users tbl_um_users1 { get; set; }
    }
}
