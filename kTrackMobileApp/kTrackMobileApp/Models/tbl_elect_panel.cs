namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_elect_panel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_elect_panel()
        {
            tbl_elect_panel_circuit = new HashSet<tbl_elect_panel_circuit>();
        }

        [Key]
        public int elect_panel_id { get; set; }

        public int property_equip_id { get; set; }

        public int? elect_panel_type_id { get; set; }

        public int? elect_panel_voltage_id { get; set; }

        public int? elect_panel_enclosure_id { get; set; }

        public int? elect_panel_mounting_id { get; set; }

        public int? elect_panel_wire_id { get; set; }

        public int? elect_panel_phase_id { get; set; }

        [Required]
        [StringLength(50)]
        public string elect_panel_name { get; set; }

        public double? elect_panel_amperes { get; set; }

        [StringLength(50)]
        public string elect_panel_aic_rating { get; set; }

        public int elect_panel_circuit_count { get; set; }

        [Column(TypeName = "ntext")]
        public string elect_panel_option { get; set; }

        [Column(TypeName = "ntext")]
        public string elect_panel_notes { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_elect_panel_circuit> tbl_elect_panel_circuit { get; set; }
    }
}
