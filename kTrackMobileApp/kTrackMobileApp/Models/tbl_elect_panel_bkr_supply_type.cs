namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_elect_panel_bkr_supply_type
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_elect_panel_bkr_supply_type()
        {
            tbl_elect_panel_bkr = new HashSet<tbl_elect_panel_bkr>();
        }

        [Key]
        public int elect_panel_bkr_supply_type_id { get; set; }

        [Required]
        [StringLength(255)]
        public string elect_panel_bkr_supply_type { get; set; }

        [Required]
        [StringLength(3)]
        public string elect_panel_bkr_supply_type_abbr { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_elect_panel_bkr> tbl_elect_panel_bkr { get; set; }
    }
}
