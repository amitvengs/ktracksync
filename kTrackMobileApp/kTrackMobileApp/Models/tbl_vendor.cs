namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_vendor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_vendor()
        {
            tbl_equipment = new HashSet<tbl_equipment>();
            tbl_equipment1 = new HashSet<tbl_equipment>();
            tbl_property_equip = new HashSet<tbl_property_equip>();
            tbl_property_equip1 = new HashSet<tbl_property_equip>();
        }

        [Key]
        public int vendor_id { get; set; }

        [Required]
        [StringLength(150)]
        public string vendor_name { get; set; }

        [StringLength(150)]
        public string vendor_contact_name { get; set; }

        [StringLength(13)]
        public string vendor_phone_number { get; set; }

        [StringLength(13)]
        public string vendor_fax_number { get; set; }

        [StringLength(150)]
        public string vendor_email_address { get; set; }

        [StringLength(50)]
        public string vendor_address_1 { get; set; }

        [StringLength(50)]
        public string vendor_address_2 { get; set; }

        [StringLength(100)]
        public string vendor_address_city { get; set; }

        [StringLength(2)]
        public string vendor_address_state { get; set; }

        [StringLength(10)]
        public string vendor_address_zip { get; set; }

        [StringLength(75)]
        public string vendor_web_site { get; set; }

        public int created_by_user_id { get; set; }

        public DateTime creation_date { get; set; }

        public bool deleted { get; set; }

        public int last_edited_by_user_id { get; set; }

        public DateTime last_edited_date { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_equipment> tbl_equipment { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_equipment> tbl_equipment1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip> tbl_property_equip { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip> tbl_property_equip1 { get; set; }
    }
}
