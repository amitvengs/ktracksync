namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_property_equip_validation_status_details
    {
        [Key]
        public int property_equip_validation_status_details_id { get; set; }

        public int validation_status_id { get; set; }

        public int property_equip_id { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        public Guid rowguid { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public bool Deleted { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public virtual tbl_property_equip tbl_property_equip { get; set; }

        public virtual tbl_validation_status tbl_validation_status { get; set; }

        public virtual tbl_um_users tbl_um_users { get; set; }
    }
}
