namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_client_property
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_client_property()
        {
            tbl_client_property_user_access_details = new HashSet<tbl_client_property_user_access_details>();
            tbl_property_project = new HashSet<tbl_property_project>();
        }

        [Key]
        public int cp_id { get; set; }

        public int client_id { get; set; }

        public int property_id { get; set; }

        [StringLength(42)]
        public string cp_suit_apt_unit { get; set; }

        [Column(TypeName = "ntext")]
        public string notes { get; set; }

        public bool is_active { get; set; }

        public int created_by_user_id { get; set; }

        public DateTime creation_date { get; set; }

        public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public virtual tbl_client tbl_client { get; set; }

        public virtual tbl_property tbl_property { get; set; }

        public virtual tbl_um_users tbl_um_users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_client_property_user_access_details> tbl_client_property_user_access_details { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_project> tbl_property_project { get; set; }
    }
}
