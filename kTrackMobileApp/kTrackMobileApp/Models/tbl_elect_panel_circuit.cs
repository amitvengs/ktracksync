namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_elect_panel_circuit
    {
        [Key]
        public int elect_panel_circuit_id { get; set; }

        public int elect_panel_id { get; set; }

        public int? elect_panel_bkr_id { get; set; }

        public int elect_panel_circuit_number { get; set; }

        public double? elect_panel_bkr_kva_phase_a { get; set; }

        public double? elect_panel_bkr_kva_phase_b { get; set; }

        public double? elect_panel_bkr_kva_phase_c { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public virtual tbl_elect_panel tbl_elect_panel { get; set; }

        public virtual tbl_elect_panel_bkr tbl_elect_panel_bkr { get; set; }

        public virtual tbl_um_users tbl_um_users { get; set; }
    }
}
