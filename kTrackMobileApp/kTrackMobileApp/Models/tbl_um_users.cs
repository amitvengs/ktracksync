namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_um_users
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_um_users()
        {
            tbl_client_property = new HashSet<tbl_client_property>();
            tbl_client_property_user_access_details = new HashSet<tbl_client_property_user_access_details>();
            tbl_client_property_user_access_details1 = new HashSet<tbl_client_property_user_access_details>();
            tbl_contact = new HashSet<tbl_contact>();
            tbl_elect_panel_bkr = new HashSet<tbl_elect_panel_bkr>();
            tbl_elect_panel_circuit = new HashSet<tbl_elect_panel_circuit>();
            tbl_equip_category = new HashSet<tbl_equip_category>();
            tbl_equipment = new HashSet<tbl_equipment>();
            tbl_origin = new HashSet<tbl_origin>();
            tbl_property = new HashSet<tbl_property>();
            tbl_property_barcode = new HashSet<tbl_property_barcode>();
            tbl_property_equip = new HashSet<tbl_property_equip>();
            tbl_property_equip_area_served = new HashSet<tbl_property_equip_area_served>();
            tbl_property_equip_elect_panel_bkr = new HashSet<tbl_property_equip_elect_panel_bkr>();
            tbl_property_equip_status = new HashSet<tbl_property_equip_status>();
            tbl_property_equip_validation_status_details = new HashSet<tbl_property_equip_validation_status_details>();
            tbl_property_loc = new HashSet<tbl_property_loc>();
            tbl_property_project = new HashSet<tbl_property_project>();
            tbl_property_project1 = new HashSet<tbl_property_project>();
            tbl_property_project_user_access_details = new HashSet<tbl_property_project_user_access_details>();
            tbl_property_project_user_access_details1 = new HashSet<tbl_property_project_user_access_details>();
            tbl_um_user_object_transaction_rights = new HashSet<tbl_um_user_object_transaction_rights>();
            tbl_um_user_object_transaction_rights1 = new HashSet<tbl_um_user_object_transaction_rights>();
            tbl_um_users1 = new HashSet<tbl_um_users>();
            tbl_validation_status = new HashSet<tbl_validation_status>();
            tbl_validation_status_work_flow_details = new HashSet<tbl_validation_status_work_flow_details>();
            tbl_validation_status_work_flow = new HashSet<tbl_validation_status_work_flow>();
        }

        [Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int user_id { get; set; }

        public int user_type_id { get; set; }

        public int user_mapping_id { get; set; }

        [Required]
        [StringLength(200)]
        public string user_login_id { get; set; }

        public bool is_active { get; set; }

        [Required]
        [StringLength(250)]
        public string logged_in_location { get; set; }

        public bool is_system_account { get; set; }

        public int? master_login_id { get; set; }

        public DateTime user_creation_date { get; set; }

        public int user_created_by { get; set; }

        public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_client_property> tbl_client_property { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_client_property_user_access_details> tbl_client_property_user_access_details { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_client_property_user_access_details> tbl_client_property_user_access_details1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_contact> tbl_contact { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_elect_panel_bkr> tbl_elect_panel_bkr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_elect_panel_circuit> tbl_elect_panel_circuit { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_equip_category> tbl_equip_category { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_equipment> tbl_equipment { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_origin> tbl_origin { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property> tbl_property { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_barcode> tbl_property_barcode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip> tbl_property_equip { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip_area_served> tbl_property_equip_area_served { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip_elect_panel_bkr> tbl_property_equip_elect_panel_bkr { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip_status> tbl_property_equip_status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip_validation_status_details> tbl_property_equip_validation_status_details { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_loc> tbl_property_loc { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_project> tbl_property_project { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_project> tbl_property_project1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_project_user_access_details> tbl_property_project_user_access_details { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_project_user_access_details> tbl_property_project_user_access_details1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_um_user_object_transaction_rights> tbl_um_user_object_transaction_rights { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_um_user_object_transaction_rights> tbl_um_user_object_transaction_rights1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_um_users> tbl_um_users1 { get; set; }

        public virtual tbl_um_users tbl_um_users2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_validation_status> tbl_validation_status { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_validation_status_work_flow_details> tbl_validation_status_work_flow_details { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_validation_status_work_flow> tbl_validation_status_work_flow { get; set; }
    }
}
