namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_document_type_hierarchy
    {
        [Key]
        public int document_type_hierarchy_id { get; set; }

        public int document_type_id { get; set; }

        public int parent_document_type_id { get; set; }

        public bool is_document_attachable { get; set; }

        public bool is_system_default { get; set; }

        public bool is_mobile_photo_enabled { get; set; }

        public int document_file_folder_property_details_id { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        public bool deleted { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }
    }
}
