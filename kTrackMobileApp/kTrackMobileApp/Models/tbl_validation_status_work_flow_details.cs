namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_validation_status_work_flow_details
    {
        [Key]
        public int validation_status_work_flow_details_id { get; set; }

        public int validation_status_work_flow_id { get; set; }

        public int validation_status_id { get; set; }

        public int validation_status_work_flow_order { get; set; }

        public bool validation_status_user_selectable { get; set; }

        public int? property_equip_status_id { get; set; }

        public bool user_created_default { get; set; }

        public bool imported_default { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public virtual tbl_property_equip_status tbl_property_equip_status { get; set; }

        public virtual tbl_um_users tbl_um_users { get; set; }

        public virtual tbl_validation_status tbl_validation_status { get; set; }

        public virtual tbl_validation_status_work_flow tbl_validation_status_work_flow { get; set; }
    }
}
