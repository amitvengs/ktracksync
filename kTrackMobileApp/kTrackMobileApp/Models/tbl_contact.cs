namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_contact
    {
        [Key]
        public int contact_id { get; set; }

        [Required]
        [StringLength(50)]
        public string contact_f_name { get; set; }

        [Required]
        [StringLength(50)]
        public string contact_l_name { get; set; }

        [StringLength(50)]
        public string contact_title { get; set; }

        [StringLength(14)]
        public string contact_phone { get; set; }

        [StringLength(6)]
        public string contact_ext { get; set; }

        [StringLength(14)]
        public string contact_cell_phone { get; set; }

        [StringLength(14)]
        public string contact_fax { get; set; }

        [StringLength(50)]
        public string contact_email { get; set; }

        [StringLength(50)]
        public string contact_add1 { get; set; }

        [StringLength(50)]
        public string contact_add2 { get; set; }

        [StringLength(50)]
        public string contact_city { get; set; }

        [StringLength(50)]
        public string contact_state { get; set; }

        [StringLength(50)]
        public string contact_zip { get; set; }

        [StringLength(50)]
        public string contact_notes { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public virtual tbl_um_users tbl_um_users { get; set; }
    }
}
