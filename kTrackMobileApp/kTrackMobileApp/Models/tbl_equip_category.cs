namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_equip_category
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_equip_category()
        {
            tbl_equipment = new HashSet<tbl_equipment>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int equip_category_id { get; set; }

        [Required]
        [StringLength(50)]
        public string equip_category_name { get; set; }

        [StringLength(20)]
        public string equip_category_acronym { get; set; }

        public bool equip_category_pm_approved { get; set; }

        [StringLength(50)]
        public string equip_category_pm_approved_by { get; set; }

        [Column(TypeName = "date")]
        public DateTime? equip_category_pm_approved_date { get; set; }

        public int equip_category_parent { get; set; }

        public int equip_category_level { get; set; }

        public bool equip_category_is_maintained { get; set; }

        public bool equip_category_is_electrical_panel { get; set; }

        public bool equip_category_is_powered { get; set; }

        public bool equip_category_is_dco { get; set; }

        public bool equip_category_is_equip_cat { get; set; }

        public bool equip_category_has_start_date { get; set; }

        public bool equip_category_has_keys { get; set; }

        public int? system_component_config_id { get; set; }

        public int? color_id { get; set; }

        public int? default_shape_id { get; set; }

        public int? custom_shape_id { get; set; }

        public int validation_status_work_flow_id { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public virtual tbl_um_users tbl_um_users { get; set; }

        public virtual tbl_validation_status_work_flow tbl_validation_status_work_flow { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_equipment> tbl_equipment { get; set; }
    }
}
