namespace kTrackMobileApp.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using Microsoft.Azure.Mobile.Server.Tables;
    public partial class kTrackContext : DbContext
    {
        public kTrackContext()
            : base("name=kTrackContext")
        {
        }

        public virtual DbSet<tbl_client> tbl_client { get; set; }
        public virtual DbSet<tbl_client_property> tbl_client_property { get; set; }
        public virtual DbSet<tbl_client_property_user_access_details> tbl_client_property_user_access_details { get; set; }
        public virtual DbSet<tbl_contact> tbl_contact { get; set; }
        public virtual DbSet<tbl_elect_panel> tbl_elect_panel { get; set; }
        public virtual DbSet<tbl_elect_panel_bkr> tbl_elect_panel_bkr { get; set; }
        public virtual DbSet<tbl_elect_panel_bkr_supply_type> tbl_elect_panel_bkr_supply_type { get; set; }
        public virtual DbSet<tbl_elect_panel_circuit> tbl_elect_panel_circuit { get; set; }
        public virtual DbSet<tbl_equip_category> tbl_equip_category { get; set; }
        public virtual DbSet<tbl_equipment> tbl_equipment { get; set; }
        public virtual DbSet<tbl_origin> tbl_origin { get; set; }
        public virtual DbSet<tbl_property> tbl_property { get; set; }
        public virtual DbSet<tbl_property_barcode> tbl_property_barcode { get; set; }
        public virtual DbSet<tbl_property_equip> tbl_property_equip { get; set; }
        public virtual DbSet<tbl_property_equip_area_served> tbl_property_equip_area_served { get; set; }
        public virtual DbSet<tbl_property_equip_elect_panel_bkr> tbl_property_equip_elect_panel_bkr { get; set; }
        public virtual DbSet<tbl_property_equip_status> tbl_property_equip_status { get; set; }
        public virtual DbSet<tbl_property_equip_validation_status_details> tbl_property_equip_validation_status_details { get; set; }
        public virtual DbSet<tbl_property_loc> tbl_property_loc { get; set; }
        public virtual DbSet<tbl_property_project> tbl_property_project { get; set; }
        public virtual DbSet<tbl_property_project_user_access_details> tbl_property_project_user_access_details { get; set; }
        public virtual DbSet<tbl_um_user_object_transaction_rights> tbl_um_user_object_transaction_rights { get; set; }
        public virtual DbSet<tbl_um_users> tbl_um_users { get; set; }
        public virtual DbSet<tbl_validation_status> tbl_validation_status { get; set; }
        public virtual DbSet<tbl_validation_status_work_flow> tbl_validation_status_work_flow { get; set; }
        public virtual DbSet<tbl_validation_status_work_flow_details> tbl_validation_status_work_flow_details { get; set; }
        public virtual DbSet<tbl_vendor> tbl_vendor { get; set; }

        public virtual DbSet<tbl_document> tbl_document { get; set; }
        public virtual DbSet<tbl_document_attachment> tbl_document_attachment { get; set; }
        public virtual DbSet<tbl_document_type> tbl_document_type { get; set; }
        public virtual DbSet<tbl_document_type_entity_hierarchy_details> tbl_document_type_entity_hierarchy_details { get; set; }
        public virtual DbSet<tbl_document_type_hierarchy> tbl_document_type_hierarchy { get; set; }
        public virtual DbSet<tbl_property_equip_validation_status_details_log> tbl_property_equip_validation_status_details_log { get; set; }

        public virtual DbSet<tbl_vendor_type> tbl_vendor_type { get; set; }
        public virtual DbSet<tbl_vendor_type_details> tbl_vendor_type_details { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add(
               new AttributeToColumnAnnotationConvention<TableColumnAttribute, string>(
                   "ServiceTableColumn", (property, attributes) => attributes.Single().ColumnType.ToString()));

            modelBuilder.Entity<tbl_client>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_client>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_client>()
                .HasMany(e => e.tbl_client_property)
                .WithRequired(e => e.tbl_client)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_client_property>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_client_property>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_client_property>()
                .HasMany(e => e.tbl_client_property_user_access_details)
                .WithRequired(e => e.tbl_client_property)
                .HasForeignKey(e => e.client_property_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_client_property>()
                .HasMany(e => e.tbl_property_project)
                .WithRequired(e => e.tbl_client_property)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_client_property_user_access_details>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_client_property_user_access_details>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_contact>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_contact>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_elect_panel>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_elect_panel>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_elect_panel>()
                .HasMany(e => e.tbl_elect_panel_circuit)
                .WithRequired(e => e.tbl_elect_panel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_elect_panel_bkr>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_elect_panel_bkr>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_elect_panel_bkr>()
                .HasMany(e => e.tbl_property_equip_elect_panel_bkr)
                .WithRequired(e => e.tbl_elect_panel_bkr)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_elect_panel_bkr_supply_type>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_elect_panel_bkr_supply_type>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_elect_panel_circuit>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_elect_panel_circuit>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_equip_category>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_equip_category>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_equip_category>()
                .HasMany(e => e.tbl_equipment)
                .WithRequired(e => e.tbl_equip_category)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_equipment>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_equipment>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_equipment>()
                .HasMany(e => e.tbl_property_equip)
                .WithRequired(e => e.tbl_equipment)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_origin>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_origin>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_origin>()
                .HasMany(e => e.tbl_equipment)
                .WithRequired(e => e.tbl_origin)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_origin>()
                .HasMany(e => e.tbl_property_equip)
                .WithRequired(e => e.tbl_origin)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_origin>()
                .HasMany(e => e.tbl_property_loc)
                .WithRequired(e => e.tbl_origin)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_property>()
                .Property(e => e.property_state)
                .IsFixedLength();

            modelBuilder.Entity<tbl_property>()
                .Property(e => e.property_lat)
                .HasPrecision(18, 6);

            modelBuilder.Entity<tbl_property>()
                .Property(e => e.property_long)
                .HasPrecision(18, 6);

            modelBuilder.Entity<tbl_property>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_property>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_property>()
                .HasMany(e => e.tbl_client_property)
                .WithRequired(e => e.tbl_property)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_property>()
                .HasMany(e => e.tbl_property_barcode)
                .WithRequired(e => e.tbl_property)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_property>()
                .HasMany(e => e.tbl_property_equip)
                .WithRequired(e => e.tbl_property)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_property>()
                .HasMany(e => e.tbl_property_loc)
                .WithRequired(e => e.tbl_property)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_property>()
                .HasMany(e => e.tbl_um_user_object_transaction_rights)
                .WithRequired(e => e.tbl_property)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_property_barcode>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_property_barcode>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_property_equip>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_property_equip>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_property_equip>()
                .HasMany(e => e.tbl_property_equip_area_served)
                .WithRequired(e => e.tbl_property_equip)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<tbl_property_equip>()
                .HasMany(e => e.tbl_property_equip_elect_panel_bkr)
                .WithRequired(e => e.tbl_property_equip)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_property_equip>()
                .HasMany(e => e.tbl_property_equip_validation_status_details)
                .WithRequired(e => e.tbl_property_equip)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_property_equip_area_served>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_property_equip_area_served>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_property_equip_elect_panel_bkr>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_property_equip_elect_panel_bkr>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_property_equip_status>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_property_equip_status>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_property_equip_status>()
                .HasMany(e => e.tbl_property_equip)
                .WithRequired(e => e.tbl_property_equip_status)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_property_loc>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_property_loc>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_property_loc>()
                .HasMany(e => e.tbl_property_equip_area_served)
                .WithRequired(e => e.tbl_property_loc)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<tbl_property_project>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_property_project>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_property_project_user_access_details>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_property_project_user_access_details>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_um_user_object_transaction_rights>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_um_user_object_transaction_rights>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_um_users>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_um_users>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_client_property)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_client_property_user_access_details)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_client_property_user_access_details1)
                .WithRequired(e => e.tbl_um_users1)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_contact)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_elect_panel_bkr)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_elect_panel_circuit)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_equip_category)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_equipment)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_origin)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_property)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_property_barcode)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_property_equip)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_property_equip_area_served)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_property_equip_elect_panel_bkr)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_property_equip_status)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_property_equip_validation_status_details)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_property_loc)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_property_project)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.last_edited_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_property_project1)
                .WithRequired(e => e.tbl_um_users1)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_property_project_user_access_details)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_property_project_user_access_details1)
                .WithRequired(e => e.tbl_um_users1)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_um_user_object_transaction_rights)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_um_user_object_transaction_rights1)
                .WithRequired(e => e.tbl_um_users1)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_um_users1)
                .WithRequired(e => e.tbl_um_users2)
                .HasForeignKey(e => e.user_created_by);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_validation_status)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_validation_status_work_flow_details)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_um_users>()
                .HasMany(e => e.tbl_validation_status_work_flow)
                .WithRequired(e => e.tbl_um_users)
                .HasForeignKey(e => e.created_by_user_id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_validation_status>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_validation_status>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_validation_status>()
                .HasMany(e => e.tbl_equipment)
                .WithRequired(e => e.tbl_validation_status)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_validation_status>()
                .HasMany(e => e.tbl_property_equip)
                .WithRequired(e => e.tbl_validation_status)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_validation_status>()
                .HasMany(e => e.tbl_property_equip_validation_status_details)
                .WithRequired(e => e.tbl_validation_status)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_validation_status>()
                .HasMany(e => e.tbl_validation_status_work_flow_details)
                .WithRequired(e => e.tbl_validation_status)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_validation_status_work_flow>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_validation_status_work_flow>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_validation_status_work_flow>()
                .HasMany(e => e.tbl_equip_category)
                .WithRequired(e => e.tbl_validation_status_work_flow)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_validation_status_work_flow>()
                .HasMany(e => e.tbl_validation_status_work_flow_details)
                .WithRequired(e => e.tbl_validation_status_work_flow)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_validation_status_work_flow_details>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_validation_status_work_flow_details>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_vendor>()
                .Property(e => e.vendor_address_city)
                .IsFixedLength();

            modelBuilder.Entity<tbl_vendor>()
                .Property(e => e.vendor_address_state)
                .IsFixedLength();

            modelBuilder.Entity<tbl_vendor>()
                .Property(e => e.vendor_address_zip)
                .IsFixedLength();

            modelBuilder.Entity<tbl_vendor>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_vendor>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_vendor>()
                .HasMany(e => e.tbl_equipment)
                .WithOptional(e => e.tbl_vendor)
                .HasForeignKey(e => e.installed_by_contractor_id);

            modelBuilder.Entity<tbl_vendor>()
                .HasMany(e => e.tbl_equipment1)
                .WithOptional(e => e.tbl_vendor1)
                .HasForeignKey(e => e.supplied_by_vendor_id);

            modelBuilder.Entity<tbl_vendor>()
                .HasMany(e => e.tbl_property_equip)
                .WithOptional(e => e.tbl_vendor)
                .HasForeignKey(e => e.installed_by_contractor_id);

            modelBuilder.Entity<tbl_vendor>()
                .HasMany(e => e.tbl_property_equip1)
                .WithOptional(e => e.tbl_vendor1)
                .HasForeignKey(e => e.supplied_by_vendor_id);

            modelBuilder.Entity<tbl_document>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_document>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_document>()
                .HasMany(e => e.tbl_document_attachment)
                .WithRequired(e => e.tbl_document)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_document_attachment>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_document_attachment>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_document_type>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_document_type>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_document_type_entity_hierarchy_details>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_document_type_entity_hierarchy_details>()
                .Property(e => e.Version)
                .IsFixedLength();
            
            modelBuilder.Entity<tbl_document_type_hierarchy>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_document_type_hierarchy>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_property_equip_validation_status_details_log>()
               .Property(e => e.Id)
               .IsUnicode(false);

            modelBuilder.Entity<tbl_property_equip_validation_status_details_log>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_vendor_type>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_vendor_type>()
                .Property(e => e.Version)
                .IsFixedLength();

            modelBuilder.Entity<tbl_vendor_type>()
                .HasMany(e => e.tbl_vendor_type_details)
                .WithRequired(e => e.tbl_vendor_type)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<tbl_vendor_type_details>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<tbl_vendor_type_details>()
                .Property(e => e.Version)
                .IsFixedLength();
        }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.VendorTypedto> VendorTypedtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.VendorTypeDetailsdto> VendorTypeDetailsdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.PropertyEquipValidationStatusDetailsLogdto> PropertyEquipValidationStatusDetailsLogdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.DocumentAttachmentdto> DocumentAttachmentdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.Documentdto> Documentdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.DocumentTypeEntityHierarchyDetailsdto> DocumentTypeEntityHierarchyDetailsdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.DocumentTypedto> DocumentTypedtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.DocumentTypeHierarchydto> DocumentTypeHierarchydtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.Propertydto> Propertydtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.Usersdto> Usersdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.ClientPropertyUserAccessDetailsdto> ClientPropertyUserAccessDetailsdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.ClientPropertydto> ClientPropertydtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.PropertyLocationdto> PropertyLocationdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.Vendordto> Vendordtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.Contactdto> Contactdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.PropertyEquipmentdto> PropertyEquipmentdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.Equipmentdto> Equipmentdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.EquipCategorydto> EquipCategorydtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.PropertyEquipmentStatusdto> PropertyEquipmentStatusdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.ValidationStatusdto> ValidationStatusdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.Origindto> Origindtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.ValidationStatusWorkFlowdto> ValidationStatusWorkFlowdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.ValidationStatusWorkFlowDetailsdto> ValidationStatusWorkFlowDetailsdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.PropertyBarcodedto> PropertyBarcodedtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.UserObjectTransactionRightsdto> UserObjectTransactionRightsdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.ElectPanelBkrdto> ElectPanelBkrdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.ElectPanelBkrSupplyTypedto> ElectPanelBkrSupplyTypedtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.ElectPanelCircuitdto> ElectPanelCircuitdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.ElectPaneldto> ElectPaneldtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.PropertyEquipElectPanelBkrdto> PropertyEquipElectPanelBkrs { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.PropertyEquipmentAreaServeddto> PropertyEquipmentAreaServeddtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.PropertyEquipValidationStatusDetailsdto> PropertyEquipValidationStatusDetails { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.PropertyProjectdto> PropertyProjectdtoes { get; set; }

        //public System.Data.Entity.DbSet<kTrackMobileApp.DataObjects.PropertyProjectUserAccessDetailsdto> PropertyProjectUserAccessDetailsdtoes { get; set; }
    }
}
