namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_validation_status
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_validation_status()
        {
            tbl_equipment = new HashSet<tbl_equipment>();
            tbl_property_equip = new HashSet<tbl_property_equip>();
            tbl_property_equip_validation_status_details = new HashSet<tbl_property_equip_validation_status_details>();
            tbl_validation_status_work_flow_details = new HashSet<tbl_validation_status_work_flow_details>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int validation_status_id { get; set; }

        [Required]
        [StringLength(50)]
        public string validation_status { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string validation_status_description { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        public Guid rowguid { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public bool Deleted { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_equipment> tbl_equipment { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip> tbl_property_equip { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip_validation_status_details> tbl_property_equip_validation_status_details { get; set; }

        public virtual tbl_um_users tbl_um_users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_validation_status_work_flow_details> tbl_validation_status_work_flow_details { get; set; }
    }
}
