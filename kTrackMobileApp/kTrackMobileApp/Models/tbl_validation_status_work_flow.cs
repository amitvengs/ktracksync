namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_validation_status_work_flow
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_validation_status_work_flow()
        {
            tbl_equip_category = new HashSet<tbl_equip_category>();
            tbl_validation_status_work_flow_details = new HashSet<tbl_validation_status_work_flow_details>();
        }

        [Key]
        public int validation_status_work_flow_id { get; set; }

        [Required]
        [StringLength(50)]
        public string validation_status_work_flow_name { get; set; }

        public bool is_default { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_equip_category> tbl_equip_category { get; set; }

        public virtual tbl_um_users tbl_um_users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_validation_status_work_flow_details> tbl_validation_status_work_flow_details { get; set; }
    }
}
