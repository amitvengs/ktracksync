namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_property
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_property()
        {
            tbl_client_property = new HashSet<tbl_client_property>();
            tbl_property_barcode = new HashSet<tbl_property_barcode>();
            tbl_property_equip = new HashSet<tbl_property_equip>();
            tbl_property_loc = new HashSet<tbl_property_loc>();
            tbl_um_user_object_transaction_rights = new HashSet<tbl_um_user_object_transaction_rights>();
        }

        [Key]
        public int property_id { get; set; }

        [Required]
        [StringLength(100)]
        public string property_name { get; set; }

        [Required]
        [StringLength(100)]
        public string property_add1 { get; set; }

        [StringLength(100)]
        public string property_add2 { get; set; }

        [Required]
        [StringLength(100)]
        public string property_city { get; set; }

        [Required]
        [StringLength(2)]
        public string property_state { get; set; }

        [Required]
        [StringLength(9)]
        public string property_zip { get; set; }

        public decimal property_lat { get; set; }

        public decimal property_long { get; set; }

        [Column(TypeName = "ntext")]
        public string notes { get; set; }

        public bool is_active { get; set; }

        public DateTime creation_date { get; set; }

        public int created_by_user_id { get; set; }

        public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_client_property> tbl_client_property { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_barcode> tbl_property_barcode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip> tbl_property_equip { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_loc> tbl_property_loc { get; set; }

        public virtual tbl_um_users tbl_um_users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_um_user_object_transaction_rights> tbl_um_user_object_transaction_rights { get; set; }
    }
}
