namespace kTrackMobileApp.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class tbl_property_equip
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_property_equip()
        {
            tbl_property_equip_area_served = new HashSet<tbl_property_equip_area_served>();
            tbl_property_equip_elect_panel_bkr = new HashSet<tbl_property_equip_elect_panel_bkr>();
            tbl_property_equip_validation_status_details = new HashSet<tbl_property_equip_validation_status_details>();
        }

        [Key]
        //[DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int property_equip_id { get; set; }

        public int property_id { get; set; }

        public int equipment_id { get; set; }

        [StringLength(150)]
        public string property_equip_serial_number { get; set; }

        [StringLength(150)]
        public string property_equip_unit_number { get; set; }

        [StringLength(150)]
        public string property_equip_revit_mark { get; set; }

        [Column(TypeName = "date")]
        public DateTime? property_equip_manufacture_date { get; set; }

        [Column(TypeName = "date")]
        public DateTime? property_equip_installation_date { get; set; }

        public bool property_equip_is_warranty { get; set; }

        public bool property_equip_is_assoc { get; set; }

        [Column(TypeName = "ntext")]
        public string property_equip_notes { get; set; }

        [Required]
        [StringLength(150)]
        public string property_equip_asset_tag { get; set; }

        [Column(TypeName = "date")]
        public DateTime? property_equip_expiration_date { get; set; }

        public int property_equip_status_id { get; set; }

        public int validation_status_id { get; set; }

        public bool property_equip_is_under_contract { get; set; }

        public int? property_loc_id { get; set; }

        public int? installed_by_contractor_id { get; set; }

        public int? supplied_by_vendor_id { get; set; }

        public int? revit_id { get; set; }

        public int origin_id { get; set; }

        public int created_by_user_id { get; set; }

        public DateTime creation_date { get; set; }

        public bool deleted { get; set; }

        public Guid rowguid { get; set; }

        [Column(TypeName = "date")]
        public DateTime? property_equip_start_date { get; set; }

        public int last_edited_by_user_id { get; set; }

        public DateTime last_edited_date { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        [StringLength(100)]
        public string Id { get; set; }

        public DateTimeOffset? UpdatedAt { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] Version { get; set; }

        public virtual tbl_equipment tbl_equipment { get; set; }

        public virtual tbl_origin tbl_origin { get; set; }

        public virtual tbl_property tbl_property { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip_area_served> tbl_property_equip_area_served { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip_elect_panel_bkr> tbl_property_equip_elect_panel_bkr { get; set; }

        public virtual tbl_property_equip_status tbl_property_equip_status { get; set; }

        public virtual tbl_validation_status tbl_validation_status { get; set; }

        public virtual tbl_property_loc tbl_property_loc { get; set; }

        public virtual tbl_um_users tbl_um_users { get; set; }

        public virtual tbl_vendor tbl_vendor { get; set; }

        public virtual tbl_vendor tbl_vendor1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_equip_validation_status_details> tbl_property_equip_validation_status_details { get; set; }
    }
}
