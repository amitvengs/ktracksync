﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Web.Http;
using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.Mobile.Server.Authentication;
using Microsoft.Azure.Mobile.Server.Config;
using kTrackMobileApp.DataObjects;
using kTrackMobileApp.Models;
using Owin;

namespace kTrackMobileApp
{
    public partial class Startup
    {
        public static void ConfigureMobileApp(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            //For more information on Web API tracing, see http://go.microsoft.com/fwlink/?LinkId=620686 
            config.EnableSystemDiagnosticsTracing();

            new MobileAppConfiguration()
                .UseDefaultConfiguration()
                .ApplyTo(config);

            #region AutoMapper
            
            AutoMapper.Mapper.Initialize(cfg =>
            {
                //tbl_client
                cfg.CreateMap<tbl_client, Clientdto>();
                cfg.CreateMap<Clientdto, tbl_client>();

                //tbl_property
                cfg.CreateMap<tbl_property, Propertydto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<Propertydto, tbl_property>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_client_property
                cfg.CreateMap<tbl_client_property, ClientPropertydto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<ClientPropertydto, tbl_client_property>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));
                
                //tbl_client_property_user_access_details
                cfg.CreateMap<tbl_client_property_user_access_details, ClientPropertyUserAccessDetailsdto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<ClientPropertyUserAccessDetailsdto, tbl_client_property_user_access_details>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_um_users
                cfg.CreateMap<tbl_um_users, Usersdto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<Usersdto, tbl_um_users>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_um_user_object_transaction_rights
                cfg.CreateMap<tbl_um_user_object_transaction_rights, UserObjectTransactionRightsdto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<UserObjectTransactionRightsdto, tbl_um_user_object_transaction_rights>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_Contact
                cfg.CreateMap<tbl_contact, Contactdto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<Contactdto, tbl_contact>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_property_barcode
                cfg.CreateMap<tbl_property_barcode, PropertyBarcodedto>()
                            .ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted))
                            .ForMember(dto => dto.MobilePropertyId, map => map.MapFrom(tbl => tbl.tbl_property.Id))
                            .ForMember(dto => dto.MobilePropertyEquipId, map => map.MapFrom(tbl => tbl.tbl_property_equip.Id));
                cfg.CreateMap<PropertyBarcodedto, tbl_property_barcode>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_property_project
                cfg.CreateMap<tbl_property_project, PropertyProjectdto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<PropertyProjectdto, tbl_property_project>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_property_project_user_access_details
                cfg.CreateMap<tbl_property_project_user_access_details, PropertyProjectUserAccessDetailsdto>();
                cfg.CreateMap<PropertyProjectUserAccessDetailsdto, tbl_property_project_user_access_details>();

                //tbl_equip_category
                cfg.CreateMap<tbl_equip_category, EquipCategorydto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<EquipCategorydto, tbl_equip_category>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_property_equip
                cfg.CreateMap<tbl_property_equip, PropertyEquipmentdto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<PropertyEquipmentdto, tbl_property_equip>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_equipment
                cfg.CreateMap<tbl_equipment, Equipmentdto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<Equipmentdto, tbl_equipment>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_property_equip_status
                cfg.CreateMap<tbl_property_equip_status, PropertyEquipmentStatusdto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<PropertyEquipmentStatusdto, tbl_property_equip_status>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_validation_status
                cfg.CreateMap<tbl_validation_status, ValidationStatusdto>();
                cfg.CreateMap<ValidationStatusdto, tbl_validation_status>();

                //tbl_origin
                cfg.CreateMap<tbl_origin, Origindto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<Origindto, tbl_origin>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_validation_status_work_flow
                cfg.CreateMap<tbl_validation_status_work_flow, ValidationStatusWorkFlowdto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<ValidationStatusWorkFlowdto, tbl_validation_status_work_flow>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_validation_status_work_flow_details
                cfg.CreateMap<tbl_validation_status_work_flow_details, ValidationStatusWorkFlowDetailsdto>()
                            .ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted))
                            .ForMember(dto => dto.MobilePropertyEquipStatusId, map => map.MapFrom(tbl => tbl.tbl_property_equip_status.Id))
                            .ForMember(dto => dto.MobileValidationStatusId, map => map.MapFrom(tbl => tbl.tbl_validation_status.Id))
                            .ForMember(dto => dto.MobileValidationStatusWorkFlowId, map => map.MapFrom(tbl => tbl.tbl_validation_status_work_flow.Id));
                cfg.CreateMap<ValidationStatusWorkFlowDetailsdto, tbl_validation_status_work_flow_details>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_vendor
                cfg.CreateMap<tbl_vendor, Vendordto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<Vendordto, tbl_vendor>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_property_loc
                cfg.CreateMap<tbl_property_loc, PropertyLocationdto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<PropertyLocationdto, tbl_property_loc>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_property_equip_area_served
                cfg.CreateMap<tbl_property_equip_area_served, PropertyEquipmentAreaServeddto>()
                        .ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted))
                        .ForMember(dto => dto.MobilePropertyEquipId, map => map.MapFrom(tbl => tbl.tbl_property_equip.Id))
                        .ForMember(dto => dto.MobilePropertyLocId, map => map.MapFrom(tbl => tbl.tbl_property_loc.Id));
                cfg.CreateMap<PropertyEquipmentAreaServeddto, tbl_property_equip_area_served>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_elect_panel
                cfg.CreateMap<tbl_elect_panel, ElectPaneldto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<ElectPaneldto, tbl_elect_panel>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_elect_panel_bkr
                cfg.CreateMap<tbl_elect_panel_bkr, ElectPanelBkrdto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<ElectPanelBkrdto, tbl_elect_panel_bkr>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_elect_panel_circuit
                cfg.CreateMap<tbl_elect_panel_circuit, ElectPanelCircuitdto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<ElectPanelCircuitdto, tbl_elect_panel_circuit>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_property_equip_elect_panel_bkr
                cfg.CreateMap<tbl_property_equip_elect_panel_bkr, PropertyEquipElectPanelBkrdto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<PropertyEquipElectPanelBkrdto, tbl_property_equip_elect_panel_bkr>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_elect_panel_bkr_supply_type
                cfg.CreateMap<tbl_elect_panel_bkr_supply_type, ElectPanelBkrSupplyTypedto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<ElectPanelBkrSupplyTypedto, tbl_elect_panel_bkr_supply_type>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_property_equip_validation_status_details
                cfg.CreateMap<tbl_property_equip_validation_status_details, PropertyEquipValidationStatusDetailsdto>()
                            .ForMember(dto => dto.MobileValidationStatusId, map => map.MapFrom(tbl => tbl.tbl_validation_status.Id))
                            .ForMember(dto => dto.MobilePropertyEquipId, map => map.MapFrom(tbl => tbl.tbl_property_equip.Id));
                cfg.CreateMap<PropertyEquipValidationStatusDetailsdto, tbl_property_equip_validation_status_details>();

                //tbl_document
                cfg.CreateMap<tbl_document, Documentdto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<Documentdto, tbl_document>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_document_attachment
                cfg.CreateMap<tbl_document_attachment, DocumentAttachmentdto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<DocumentAttachmentdto, tbl_document_attachment>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_document_type_entity_hierarchy_details
                cfg.CreateMap<tbl_document_type_entity_hierarchy_details, DocumentTypeEntityHierarchyDetailsdto>();
                cfg.CreateMap<DocumentTypeEntityHierarchyDetailsdto, tbl_document_type_entity_hierarchy_details>();

                //tbl_document_type_hierarchy
                cfg.CreateMap<tbl_document_type_hierarchy, DocumentTypeHierarchydto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<DocumentTypeHierarchydto, tbl_document_type_hierarchy>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));
                
                //tbl_document_type
                cfg.CreateMap<tbl_document_type, DocumentTypedto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<DocumentTypedto, tbl_document_type>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_property_equip_validation_status_details_log
                cfg.CreateMap<tbl_property_equip_validation_status_details_log, PropertyEquipValidationStatusDetailsLogdto>();
                cfg.CreateMap<PropertyEquipValidationStatusDetailsLogdto, tbl_property_equip_validation_status_details_log>();

                //tbl_vendor_type
                cfg.CreateMap<tbl_vendor_type, VendorTypedto>().ForMember(dto => dto.Deleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<VendorTypedto, tbl_vendor_type>().ForMember(tbl => tbl.deleted, map => map.MapFrom(dto => dto.Deleted));

                //tbl_vendor_type_details
                cfg.CreateMap<tbl_vendor_type_details, VendorTypeDetailsdto>()
                    .ForMember(dto => dto.MobileVendorTypeId, map => map.MapFrom(tbl => tbl.tbl_vendor_type.Id));
                cfg.CreateMap<VendorTypeDetailsdto, tbl_vendor_type_details>();
            });

            #endregion

            // Use Entity Framework Code First to create database tables based on your DbContext
            //Database.SetInitializer(new dev_ma_ktrackInitializer());

            // To prevent Entity Framework from modifying your database schema, use a null database initializer
            Database.SetInitializer<kTrackContext>(null);

            MobileAppSettingsDictionary settings = config.GetMobileAppSettingsProvider().GetMobileAppSettings();

            if (string.IsNullOrEmpty(settings.HostName))
            {
                // This middleware is intended to be used locally for debugging. By default, HostName will
                // only have a value when running in an App Service application.
                app.UseAppServiceAuthentication(new AppServiceAuthenticationOptions
                {
                    SigningKey = ConfigurationManager.AppSettings["SigningKey"],
                    ValidAudiences = new[] { ConfigurationManager.AppSettings["ValidAudience"] },
                    ValidIssuers = new[] { ConfigurationManager.AppSettings["ValidIssuer"] },
                    TokenHandler = config.GetAppServiceTokenHandler()
                });
            }
            app.UseWebApi(config);
        }
    }

    //public class dev_ma_ktrackInitializer : CreateDatabaseIfNotExists<dev_ma_ktrackContext>
    //{
    //    protected override void Seed(dev_ma_ktrackContext context)
    //    {
    //        List<TodoItem> todoItems = new List<TodoItem>
    //        {
    //            new TodoItem { Id = Guid.NewGuid().ToString(), Text = "First item", Complete = false },
    //            new TodoItem { Id = Guid.NewGuid().ToString(), Text = "Second item", Complete = false },
    //        };

    //        foreach (TodoItem todoItem in todoItems)
    //        {
    //            context.Set<TodoItem>().Add(todoItem);
    //        }

    //        base.Seed(context);
    //    }
    //}
}

