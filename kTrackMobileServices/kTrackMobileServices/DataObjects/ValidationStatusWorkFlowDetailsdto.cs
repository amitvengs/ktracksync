﻿using Microsoft.WindowsAzure.Mobile.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileServices.DataObjects
{
    public class ValidationStatusWorkFlowDetailsdto : EntityData
    {
        public int validation_status_work_flow_details_id { get; set; }
        public int validation_status_work_flow_id { get; set; }
        public int validation_status_id { get; set; }
        public int validation_status_work_flow_order { get; set; }
        public bool validation_status_user_selectable { get; set; }
        public Nullable<int> property_equip_status_id { get; set; }
        public bool user_created_default { get; set; }
        public bool imported_default { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public bool IsDeleted { get; set; }
        public System.Guid rowguid { get; set; }
        //public bool DeletedMS { get; set; }
    }
}