﻿using Microsoft.WindowsAzure.Mobile.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileServices.DataObjects
{
    public class PropertyBarcodedto : EntityData
    {
        public int barcode_id { get; set; }
        public string barcode { get; set; }
        public int mapping_id { get; set; }
        public int mapping_type_id { get; set; }
        public int property_id { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public bool IsDeleted { get; set; }
        public System.Guid rowguid { get; set; }
        //public bool DeletedMS { get; set; }
    }
}