﻿using Microsoft.WindowsAzure.Mobile.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileServices.DataObjects
{
    public class ElectPanelBkrdto : EntityData
    {
        public int elect_panel_bkr_id { get; set; }
        public Nullable<int> elect_panel_bkr_supply_type_id { get; set; }
        public string elect_panel_bkr_desc { get; set; }
        public Nullable<int> elect_panel_bkr_p { get; set; }
        public Nullable<double> elect_panel_bkr_trip { get; set; }
        public Nullable<int> elect_panel_bkr_load_type { get; set; }
        public string elect_panel_bkr_wiring_no { get; set; }
        public string elect_panel_bkr_wiring_size { get; set; }
        public string elect_panel_bkr_wiring_ground { get; set; }
        public string elect_panel_bkr_wiring_conduit_size { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public bool IsDeleted { get; set; }
        public System.Guid rowguid { get; set; }
        //public bool DeletedMS { get; set; }
    }
}