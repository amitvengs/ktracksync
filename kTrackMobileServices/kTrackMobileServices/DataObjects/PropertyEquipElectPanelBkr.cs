﻿using Microsoft.WindowsAzure.Mobile.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileServices.DataObjects
{
    public class PropertyEquipElectPanelBkr : EntityData
    {
        public int property_equip_elect_panel_bkr_id { get; set; }
        public int property_equip_id { get; set; }
        public int elect_panel_bkr_id { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public bool IsDeleted { get; set; }
        public System.Guid rowguid { get; set; }
        //public bool DeletedMS { get; set; }

    }
}