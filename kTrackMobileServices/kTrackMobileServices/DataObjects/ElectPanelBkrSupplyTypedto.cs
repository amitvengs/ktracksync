﻿using Microsoft.WindowsAzure.Mobile.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileServices.DataObjects
{
    public class ElectPanelBkrSupplyTypedto : EntityData
    {
        public int elect_panel_bkr_supply_type_id { get; set; }
        public string elect_panel_bkr_supply_type { get; set; }
        public string elect_panel_bkr_supply_type_abbr { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public bool IsDeleted { get; set; }
        public System.Guid rowguid { get; set; }
        //public bool DeletedMS { get; set; }
    }
}