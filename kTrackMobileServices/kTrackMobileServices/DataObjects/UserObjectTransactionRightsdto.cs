﻿using Microsoft.WindowsAzure.Mobile.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileServices.DataObjects
{
    public class UserObjectTransactionRightsdto : EntityData
    {
        public int um_user_object_transaction_rights_id { get; set; }
        public int um_user_object_transaction_details_id { get; set; }
        public int user_id { get; set; }
        public int property_id { get; set; }
        public int created_by_user_id { get; set; }
        public System.DateTime creation_date { get; set; }
        public bool IsDeleted { get; set; }
        public System.Guid rowguid { get; set; }
        //public bool DeletedMS { get; set; }
    }
}