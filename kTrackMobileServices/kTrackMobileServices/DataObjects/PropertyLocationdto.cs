﻿using Microsoft.WindowsAzure.Mobile.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileServices.DataObjects
{
    public class PropertyLocationdto : EntityData
    {
        public int property_loc_id { get; set; }
        public int property_id { get; set; }
        public string property_loc_name { get; set; }
        public string property_loc_number { get; set; }
        public string property_loc_const_number { get; set; }
        public Nullable<double> property_loc_volume { get; set; }
        public Nullable<double> property_loc_perimiter { get; set; }
        public Nullable<double> property_loc_area { get; set; }
        public Nullable<double> property_loc_ceiling_height { get; set; }
        public Nullable<double> property_loc_occupancy_factor { get; set; }
        public string property_loc_sign_message { get; set; }
        public Nullable<int> property_department_id { get; set; }
        public int property_loc_level_id { get; set; }
        public Nullable<int> property_loc_type_id { get; set; }
        public Nullable<int> property_loc_zone_id { get; set; }
        public int origin_id { get; set; }
        public Nullable<int> revit_id { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public bool IsDeleted { get; set; }
        public System.Guid rowguid { get; set; }
        public int last_edited_by_user_id { get; set; }
        public System.DateTime last_edited_date { get; set; }
        //public bool DeletedMS { get; set; }
    }
}