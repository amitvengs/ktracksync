﻿using Microsoft.WindowsAzure.Mobile.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileServices.DataObjects
{
    public class Propertydto : EntityData
    {
        public int property_id { get; set; }
        public string property_name { get; set; }
        public string property_add1 { get; set; }
        public string property_add2 { get; set; }
        public string property_city { get; set; }
        public string property_state { get; set; }
        public string property_zip { get; set; }
        public decimal property_lat { get; set; }
        public decimal property_long { get; set; }
        public string notes { get; set; }
        public bool is_active { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public bool IsDeleted { get; set; }
        public System.Guid rowguid { get; set; }
    }
}