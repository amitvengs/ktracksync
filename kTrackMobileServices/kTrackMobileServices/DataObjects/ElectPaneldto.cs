﻿using Microsoft.WindowsAzure.Mobile.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileServices.DataObjects
{
    public class ElectPaneldto : EntityData
    {
        public int elect_panel_id { get; set; }
        public int property_equip_id { get; set; }
        public Nullable<int> elect_panel_type_id { get; set; }
        public Nullable<int> elect_panel_voltage_id { get; set; }
        public Nullable<int> elect_panel_enclosure_id { get; set; }
        public Nullable<int> elect_panel_mounting_id { get; set; }
        public Nullable<int> elect_panel_wire_id { get; set; }
        public Nullable<int> elect_panel_phase_id { get; set; }
        public string elect_panel_name { get; set; }
        public Nullable<double> elect_panel_amperes { get; set; }
        public string elect_panel_aic_rating { get; set; }
        public int elect_panel_circuit_count { get; set; }
        public string elect_panel_option { get; set; }
        public string elect_panel_notes { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public bool IsDeleted { get; set; }
        public System.Guid rowguid { get; set; }
        //public bool DeletedMS { get; set; }
    }
}