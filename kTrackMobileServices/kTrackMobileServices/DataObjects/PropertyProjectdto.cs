﻿using Microsoft.WindowsAzure.Mobile.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kTrackMobileServices.DataObjects
{
    public class PropertyProjectdto : EntityData
    {
        public int property_project_id { get; set; }
        public int cp_id { get; set; }
        public string property_project_number { get; set; }
        public string property_project_name { get; set; }
        public string property_project_desc { get; set; }
        public string property_project_cm_gc_number { get; set; }
        public int property_project_status_id { get; set; }
        public int entity_work_flow_id { get; set; }
        public int last_edited_by_user_id { get; set; }
        public System.DateTime last_edited_date { get; set; }
        public int created_by_user_id { get; set; }
        public System.DateTime creation_date { get; set; }
        public bool IsDeleted { get; set; }
        public System.Guid rowguid { get; set; }
        //public bool DeletedMS { get; set; }
    }
}