﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Web.Http;
using kTrackMobileServices.DataObjects;
using kTrackMobileServices.Models;
using Microsoft.WindowsAzure.Mobile.Service;

namespace kTrackMobileServices
{
    public static class WebApiConfig
    {
        public static void Register()
        {
            // Use this class to set configuration options for your mobile service
            ConfigOptions options = new ConfigOptions();

            // Use this class to set WebAPI configuration options
            HttpConfiguration config = ServiceConfig.Initialize(new ConfigBuilder(options));

            // To display errors in the browser during development, uncomment the following
            // line. Comment it out again when you deploy your service for production use.
            // config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;

            //Database.SetInitializer(new MobileServiceInitializer());

            AutoMapper.Mapper.Initialize(cfg =>
            {
                // Mapping from database type to client type

                //tbl_client
                cfg.CreateMap<tbl_client, Clientdto>();
                cfg.CreateMap<Clientdto, tbl_client>();

                //tbl_property
                cfg.CreateMap<tbl_property, Propertydto>().ForMember(propertydto => propertydto.IsDeleted, map => map.MapFrom(tblproperty => tblproperty.deleted));
                cfg.CreateMap<Propertydto, tbl_property>().ForMember(tblproperty => tblproperty.DeletedMS, map => map.MapFrom(propertydto => propertydto.Deleted));

                //tbl_client_property
                cfg.CreateMap<tbl_client_property, ClientPropertydto>()
                .ForMember(clientpropertydto => clientpropertydto.IsDeleted, map => map.MapFrom(tblclientproperty => tblclientproperty.deleted));
                cfg.CreateMap<ClientPropertydto, tbl_client_property>()
                .ForMember(tblclientproperty => tblclientproperty.DeletedMS, map => map.MapFrom(clientpropertydto => clientpropertydto.Deleted));

                //tbl_client_property_user_access_details
                cfg.CreateMap<tbl_client_property_user_access_details, ClientPropertyUserAccessDetailsdto>()
                .ForMember(clientpropertyuseraccessdto => clientpropertyuseraccessdto.IsDeleted, map => map.MapFrom(tblclientpropertyuseraccess => tblclientpropertyuseraccess.deleted));
                cfg.CreateMap<ClientPropertyUserAccessDetailsdto, tbl_client_property_user_access_details>()
                .ForMember(tblclientpropertyuseraccess => tblclientpropertyuseraccess.DeletedMS, map => map.MapFrom(clientpropertyuseraccessdto => clientpropertyuseraccessdto.Deleted));

                //tbl_um_users
                cfg.CreateMap<tbl_um_users, Usersdto>().ForMember(userdto => userdto.IsDeleted, map => map.MapFrom(tbluser => tbluser.deleted));
                cfg.CreateMap<Usersdto, tbl_um_users>().ForMember(tbluser => tbluser.DeletedMS, map => map.MapFrom(userdto => userdto.Deleted));
                
                //tbl_um_user_object_transaction_rights
                cfg.CreateMap<tbl_um_user_object_transaction_rights, UserObjectTransactionRightsdto>()
                .ForMember(userrightdto => userrightdto.IsDeleted, map => map.MapFrom(tblclientproperty => tblclientproperty.deleted));
                cfg.CreateMap<UserObjectTransactionRightsdto, tbl_um_user_object_transaction_rights>()
                .ForMember(tblclientproperty => tblclientproperty.DeletedMS, map => map.MapFrom(userrightdto => userrightdto.Deleted));

                //tbl_Contact
                cfg.CreateMap<tbl_contact, Contactdto>().ForMember(dto => dto.IsDeleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<Contactdto, tbl_contact>().ForMember(tbl => tbl.DeletedMS, map => map.MapFrom(dto => dto.Deleted));

                //tbl_property_barcode
                cfg.CreateMap<tbl_property_barcode, PropertyBarcodedto>().ForMember(dto => dto.IsDeleted, map => map.MapFrom(tbl => tbl.deleted));
                cfg.CreateMap<PropertyBarcodedto, tbl_property_barcode>().ForMember(tbl => tbl.DeletedMS, map => map.MapFrom(dto => dto.Deleted));


            });
        }
    }

    //public class MobileServiceInitializer : DropCreateDatabaseIfModelChanges<MobileServiceContext>
    //{
    //    protected override void Seed(MobileServiceContext context)
    //    {
    //        List<TodoItem> todoItems = new List<TodoItem>
    //        {
    //            new TodoItem { Id = Guid.NewGuid().ToString(), Text = "First item", Complete = false },
    //            new TodoItem { Id = Guid.NewGuid().ToString(), Text = "Second item", Complete = false },
    //        };

    //        foreach (TodoItem todoItem in todoItems)
    //        {
    //            context.Set<TodoItem>().Add(todoItem);
    //        }

    //        base.Seed(context);
    //    }
    //}
}

