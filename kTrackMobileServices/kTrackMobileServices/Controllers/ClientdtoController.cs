﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.WindowsAzure.Mobile.Service;
using kTrackMobileServices.DataObjects;
using kTrackMobileServices.Models;

namespace kTrackMobileServices.Controllers
{
    public class ClientdtoController : TableController<Clientdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackEntities context = new kTrackEntities();
            //DomainManager = new EntityDomainManager<tblClientdto>(context, Request, Services);
            DomainManager = new SimpleMappedEntityDomainManager<Clientdto, tbl_client>(context, Request, Services, Item => Item.Id);
        }

        // GET tables/tblClientdto
        public IQueryable<Clientdto> GetAlltblClientdto()
        {
            return Query(); 
        }

        // GET tables/tblClientdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Clientdto> GettblClientdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/tblClientdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Clientdto> PatchtblClientdto(string id, Delta<Clientdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/tblClientdto
        public async Task<IHttpActionResult> PosttblClientdto(Clientdto item)
        {
            Clientdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/tblClientdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletetblClientdto(string id)
        {
             return DeleteAsync(id);
        }

    }
}