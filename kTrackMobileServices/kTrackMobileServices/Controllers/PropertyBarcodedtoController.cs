﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.WindowsAzure.Mobile.Service;
using kTrackMobileServices.DataObjects;
using kTrackMobileServices.Models;

namespace kTrackMobileServices.Controllers
{
    public class PropertyBarcodedtoController : TableController<PropertyBarcodedto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackEntities context = new kTrackEntities();
            //DomainManager = new EntityDomainManager<PropertyBarcodedto>(context, Request, Services);
            DomainManager = new SimpleMappedEntityDomainManager<PropertyBarcodedto, tbl_property_barcode>(context, Request, Services, Item => Item.Id);
        }

        // GET tables/PropertyBarcodedto
        public IQueryable<PropertyBarcodedto> GetAllPropertyBarcodedto()
        {
            return Query(); 
        }

        // GET tables/PropertyBarcodedto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<PropertyBarcodedto> GetPropertyBarcodedto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/PropertyBarcodedto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<PropertyBarcodedto> PatchPropertyBarcodedto(string id, Delta<PropertyBarcodedto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/PropertyBarcodedto
        public async Task<IHttpActionResult> PostPropertyBarcodedto(PropertyBarcodedto item)
        {
            PropertyBarcodedto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/PropertyBarcodedto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletePropertyBarcodedto(string id)
        {
             return DeleteAsync(id);
        }

    }
}