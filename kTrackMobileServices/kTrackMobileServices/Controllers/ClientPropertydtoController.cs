﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.WindowsAzure.Mobile.Service;
using kTrackMobileServices.DataObjects;
using kTrackMobileServices.Models;

namespace kTrackMobileServices.Controllers
{
    public class ClientPropertydtoController : TableController<ClientPropertydto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackEntities context = new kTrackEntities();
            //DomainManager = new EntityDomainManager<ClientPropertydto>(context, Request, Services);
            DomainManager = new SimpleMappedEntityDomainManager<ClientPropertydto, tbl_client_property>(context, Request, Services, Item => Item.Id);
        }

        // GET tables/ClientPropertydto
        public IQueryable<ClientPropertydto> GetAllClientPropertydto()
        {
            return Query(); 
        }

        // GET tables/ClientPropertydto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<ClientPropertydto> GetClientPropertydto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/ClientPropertydto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<ClientPropertydto> PatchClientPropertydto(string id, Delta<ClientPropertydto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/ClientPropertydto
        public async Task<IHttpActionResult> PostClientPropertydto(ClientPropertydto item)
        {
            ClientPropertydto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/ClientPropertydto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteClientPropertydto(string id)
        {
             return DeleteAsync(id);
        }

    }
}