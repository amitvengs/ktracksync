﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.WindowsAzure.Mobile.Service;
using kTrackMobileServices.DataObjects;
using kTrackMobileServices.Models;

namespace kTrackMobileServices.Controllers
{
    public class UserObjectTransactionRightsdtoController : TableController<UserObjectTransactionRightsdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackEntities context = new kTrackEntities();
            //DomainManager = new EntityDomainManager<UserObjectTransactionRightsdto>(context, Request, Services);
            DomainManager = new SimpleMappedEntityDomainManager<UserObjectTransactionRightsdto, tbl_um_user_object_transaction_rights>(context, Request, Services, Item => Item.Id);
        }

        // GET tables/UserObjectTransactionRightsdto
        public IQueryable<UserObjectTransactionRightsdto> GetAllUserObjectTransactionRightsdto()
        {
            return Query(); 
        }

        // GET tables/UserObjectTransactionRightsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<UserObjectTransactionRightsdto> GetUserObjectTransactionRightsdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/UserObjectTransactionRightsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<UserObjectTransactionRightsdto> PatchUserObjectTransactionRightsdto(string id, Delta<UserObjectTransactionRightsdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/UserObjectTransactionRightsdto
        public async Task<IHttpActionResult> PostUserObjectTransactionRightsdto(UserObjectTransactionRightsdto item)
        {
            UserObjectTransactionRightsdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/UserObjectTransactionRightsdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteUserObjectTransactionRightsdto(string id)
        {
             return DeleteAsync(id);
        }

    }
}