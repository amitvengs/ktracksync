﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.WindowsAzure.Mobile.Service;
using kTrackMobileServices.DataObjects;
using kTrackMobileServices.Models;

namespace kTrackMobileServices.Controllers
{
    public class UsersdtoController : TableController<Usersdto>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            kTrackEntities context = new kTrackEntities();
            //DomainManager = new EntityDomainManager<Usersdto>(context, Request, Services);
            DomainManager = new SimpleMappedEntityDomainManager<Usersdto, tbl_um_users>(context, Request, Services, Item => Item.Id);
        }

        // GET tables/Usersdto
        public IQueryable<Usersdto> GetAllUsersdto()
        {
            return Query(); 
        }

        // GET tables/Usersdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Usersdto> GetUsersdto(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Usersdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Usersdto> PatchUsersdto(string id, Delta<Usersdto> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Usersdto
        public async Task<IHttpActionResult> PostUsersdto(Usersdto item)
        {
            Usersdto current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Usersdto/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeleteUsersdto(string id)
        {
             return DeleteAsync(id);
        }

    }
}