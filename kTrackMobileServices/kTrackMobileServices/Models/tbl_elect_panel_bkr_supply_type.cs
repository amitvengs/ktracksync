//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace kTrackMobileServices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_elect_panel_bkr_supply_type
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_elect_panel_bkr_supply_type()
        {
            this.tbl_elect_panel_bkr = new HashSet<tbl_elect_panel_bkr>();
        }
    
        public int elect_panel_bkr_supply_type_id { get; set; }
        public string elect_panel_bkr_supply_type { get; set; }
        public string elect_panel_bkr_supply_type_abbr { get; set; }
        public System.DateTime creation_date { get; set; }
        public int created_by_user_id { get; set; }
        public bool deleted { get; set; }
        public System.Guid rowguid { get; set; }
        public System.DateTimeOffset CreatedAt { get; set; }
        public bool DeletedMS { get; set; }
        public string Id { get; set; }
        public Nullable<System.DateTimeOffset> UpdatedAt { get; set; }
        public byte[] Version { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_elect_panel_bkr> tbl_elect_panel_bkr { get; set; }
    }
}
