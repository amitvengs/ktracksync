//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace kTrackMobileServices.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_client_property
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_client_property()
        {
            this.tbl_client_property_user_access_details = new HashSet<tbl_client_property_user_access_details>();
            this.tbl_property_project = new HashSet<tbl_property_project>();
        }
    
        public int cp_id { get; set; }
        public int client_id { get; set; }
        public int property_id { get; set; }
        public string cp_suit_apt_unit { get; set; }
        public string notes { get; set; }
        public bool is_active { get; set; }
        public int created_by_user_id { get; set; }
        public System.DateTime creation_date { get; set; }
        public bool deleted { get; set; }
        public System.Guid rowguid { get; set; }
        public System.DateTimeOffset CreatedAt { get; set; }
        public bool DeletedMS { get; set; }
        public string Id { get; set; }
        public Nullable<System.DateTimeOffset> UpdatedAt { get; set; }
        public byte[] Version { get; set; }
    
        public virtual tbl_client tbl_client { get; set; }
        public virtual tbl_property tbl_property { get; set; }
        public virtual tbl_um_users tbl_um_users { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_client_property_user_access_details> tbl_client_property_user_access_details { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_property_project> tbl_property_project { get; set; }
    }
}
