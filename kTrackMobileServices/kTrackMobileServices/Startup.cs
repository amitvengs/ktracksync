﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(kTrackMobileServices.App_Start.Startup))]
namespace kTrackMobileServices.App_Start
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureMobileApp(app);
        }
    }
}